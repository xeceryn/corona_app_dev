#!/bin/bash

BUILD_VERSION=4.1.2

current_dir=`pwd`
d=`date +%m_%d_%Y`

cd android
#rm -rf app/build/outputs/apk/release
#./gradlew app:assembleRelease
./gradlew bundleRelease
#cd app/build/outputs/apk/release/
#mv app-release.apk OTUCHAT-v.${BUILD_VERSION}.apk
#open .
#echo "android/app/build/outputs/apk/release/"
echo "android/app/build/outputs/bundle/release/"
cd ${current_dir}
