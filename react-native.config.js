module.exports = {
    project: {
      ios: {},
      android: {}, // grouped into "project"
    },
    assets: ["./assets/fonts/"], // stays the same
    dependencies: {
      'react-native-track-player': {
        platforms: {
          ios: null // disable iOS platform, other platforms will still autolink if provided
        }
      }
    }
  };