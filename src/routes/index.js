import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';

import {notificationManager} from '../helper/NotificationManager';

import PhoneAuthTest from '../containers/Auth/PhoneAuthTest';

import Introduction from '../containers/Introduction';
import Login from '../containers/Auth/Login';
import RegisterVisitor from '../containers/Auth/Register/Visitor';
import RegisterKaryawan from '../containers/Auth/Register/Karyawan';
import OtpCheck from '../containers/Auth/Otp';
import Permissions from '../containers/Permissions';
import Home from '../containers/Home';
import Informasi from '../containers/Informasi';
import ModalPermissions from '../containers/ModalPermissions';
import RiwayatInteraksi from '../containers/RiwayatInteraksi';
import HealthDeclaration from '../containers/HealthDeclaration';

const Stack = createStackNavigator();
function Root() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Informasi"
        component={Informasi}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ModalPermissions"
        component={ModalPermissions}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="RiwayatInteraksi"
        component={RiwayatInteraksi}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="HealthDeclaration"
        component={HealthDeclaration}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}
export default class Routes extends Component {
  constructor(props) {
    super(props);
    this.localNotify = null;
    this.senderiD = '333432402454';
    this.state = {
      token: '',
      tokenFCM: '',
    };
  }
  async componentDidMount() {
    const userData = await AsyncStorage.getItem('token');
    // const userObj = JSON.parse(userData)
    if (userData) {
      this.setState({
        token: userData,
      });
    }
    // getFCMTOken
    this.getTokenFCM();
    //PushNotif
    this.localNotify = notificationManager;
    this.localNotify.configure(
      this.onRegister,
      this.onNotification,
      this.onOpenNotification,
      this.senderiD,
    );
  }
  onRegister(token) {
    // console.log('[Notification] Registered', token.token);
  }

  onNotification(notify) {
    // console.log('[Notification] onNotification', notify);
  }

  onOpenNotification(notify) {
    // console.log('[Notification] onOpenNotification', notify);
  }
  getTokenFCM = () => {
    AsyncStorage.getItem('tokenFCM', (error, result) => {
      if (result) {
        // alert(result)
        this.setState({
          TokenFCM: result,
        });
      }
    });
  };

  componentWillUnmount() {
    console.log('APP KILL');
  }

  render() {
    const {token} = this.state;
    return (
      <NavigationContainer>
        <Stack.Navigator>
          {token === '' ? (
            <>
              <Stack.Screen
                name="Introduction"
                component={Introduction}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="Login"
                component={Login}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="RegisterVisitor"
                component={RegisterVisitor}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="RegisterKaryawan"
                component={RegisterKaryawan}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="OtpCheck"
                component={OtpCheck}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="Permissions"
                component={Permissions}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="Root"
                component={Root}
                options={{
                  headerShown: false,
                }}
              />
              <Stack.Screen
                name="Test"
                component={PhoneAuthTest}
                options={{
                  headerShown: false,
                }}
              />
            </>
          ) : (
            <Stack.Screen
              name="Root"
              component={Root}
              options={{
                headerShown: false,
              }}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
