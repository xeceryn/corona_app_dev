import React from 'react';
import {
  Modal,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import {getHeight, getWidth} from '../../helper/init';
import {colors} from '../../helper/color';
import {RoleImage} from '../../helper/image';

const RoleModal = ({
  visible,
  onClose,
  visitorPress,
  employeePress,
  language,
}) => {
  return (
    <Modal visible={visible} animationType={'fade'} transparent={true}>
      <TouchableWithoutFeedback onPress={onClose}>
        <View style={styles.container}>
          <View style={styles.containerContent}>
            <Text style={styles.contentHeader}>
              {language == 'id' ? `Bergabung sebagai` : `Join as`}
            </Text>
            <View style={styles.containerButton}>
              <TouchableOpacity style={styles.button} onPress={employeePress}>
                <Image source={RoleImage.Karwayan} />
                <Text style={styles.buttonName}>
                  {language == 'id' ? `Karyawan` : `the employee`}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={visitorPress}>
                <Image source={RoleImage.Visitor} />
                <Text style={styles.buttonName}>
                  {language == 'id' ? `Non Karyawan` : `visitor`}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};
export default RoleModal;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContent: {
    height: getHeight(45),
    width: getWidth(90),
    backgroundColor: '#FFFFFF',
    borderRadius: getWidth(5),
    padding: getWidth(4),
  },
  contentHeader: {
    color: colors.night_rider,
    fontSize: 16,
    marginHorizontal: '5%',
    marginVertical: '5%',
    // fontFamily: 'Cairo-Bold'
    fontWeight: 'bold',
  },
  containerButton: {
    marginHorizontal: '5%',
    marginVertical: getHeight(8),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    // borderColor: colors.dim_gray,
    // borderWidth: 1,
    width: getWidth(30),
    height: getHeight(15),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonName: {
    color: colors.night_rider,
    fontSize: 16,
    fontWeight: 'bold',
    // fontFamily: 'Cairo-Bold'
  },
});
