import React from 'react'
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native'

import { colors } from '../../helper/color'

const RoundedButton = ({ onPress, title, bottom }) => {
    return (
        <View style={styles.containerButton(bottom)}>
            <TouchableOpacity 
                style={styles.roundedButton}
                onPress={onPress}
                >
                <Text style={styles.titleColor}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}
export default RoundedButton
const styles = StyleSheet.create({
    containerButton: bottom => ({
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: '100%',
        bottom
    }),
    roundedButton: {
        backgroundColor: colors.red,
        borderRadius: 15,
        height: 45,
        width: '82%',
        justifyContent: 'center'
    },
    titleColor: {
        color: colors.white,
        alignSelf: 'center',
        fontWeight: 'bold'
        // fontFamily: 'Cairo-Bold'
    }
})