import React from 'react';
import {
  Modal,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from 'react-native';

import {getHeight, getWidth} from '../../helper/init';
import {colors} from '../../helper/color';
import {Flag} from '../../helper/image';

const DATA = [
  {
    id: '1',
    title: 'Indonesia',
    flag: Flag.IND,
    language: 'id',
  },
  {
    id: '2',
    title: 'English',
    flag: Flag.ENG,
    language: 'en',
  },
];

const RoleModal = ({visible, onClose, onPress, language}) => {
  return (
    <Modal visible={visible} animationType={'fade'} transparent={true}>
      <TouchableWithoutFeedback onPress={onClose}>
        <View style={styles.container}>
          <View style={styles.containerContent}>
            <Text style={styles.contentHeader}>
              {language == 'id' ? `Pilih Bahasa` : `Select Language`}
            </Text>
            <View style={styles.containerButton}>
              <FlatList
                data={DATA}
                renderItem={({item}) => (
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      marginVertical: getHeight(0.2),
                    }}
                    onPress={() => {
                      onClose(false);
                      onPress(item.language);
                    }}>
                    <View
                      style={{
                        width: getWidth(65),
                        padding: 5,
                        borderBottomLeftRadius: 8,
                        borderTopLeftRadius: 8,
                        marginBottom: 5,
                        backgroundColor:
                          item.language == language ? '#FCECEC' : '#FFF',
                      }}>
                      <Text
                        style={{
                          fontWeight: item.language == language ? 'bold' : 'normal',
                        }}>
                        {item.title}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: getWidth(8),
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginBottom: 5,
                        padding: 5,
                        borderBottomRightRadius: 8,
                        borderTopRightRadius: 8,
                        backgroundColor:
                          item.language == language ? '#FCECEC' : '#FFF',
                      }}>
                      <View
                        style={{
                          height: getWidth(5.2),
                          width: getWidth(5.2),
                          borderRadius: getWidth(5.2),
                          backgroundColor: '#000',
                          position: 'absolute',
                        }}
                      />
                      <Image
                        source={item.flag}
                        style={{
                          height: getWidth(5),
                          width: getWidth(5),
                          borderWidth: 1,
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                )}
                keyExtractor={item => item.id}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};
export default RoleModal;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContent: {
    height: getHeight(25),
    width: getWidth(90),
    backgroundColor: '#FFFFFF',
    borderRadius: getWidth(5),
    padding: getWidth(4),
  },
  contentHeader: {
    color: colors.night_rider,
    fontSize: 16,
    marginHorizontal: '5%',
    marginVertical: '5%',
    // fontFamily: 'Cairo-Bold'
    fontWeight: 'bold',
  },
  containerButton: {
    marginHorizontal: '5%',
    marginVertical: getHeight(1),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
