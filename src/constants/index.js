// Developtment 
// export const BASE_URL = 'https://corona.sukowidodo.com/api/';
// Production
export const BASE_URL = 'https://coronadev.eisbetech.com/api/';
export const SEND_PHONE = 'send_phone';
export const SEND_OTP = 'confirm_otp';
export const REGISTER = 'register';
export const REG_EMPLOYEE = 'send_employee_code';
export const INFORMASI = 'profile';
export const TRACKING = 'v2/tracking';
export const PERUSAHAAN = 'perusahaan';
export const HUBUNGAN = 'hubungan';
export const TRACKINGDATA = 'tracking_data';
export const UPDATEAPP = 'version'
export const RIWAYATINTERAKSI = 'get-tracking-data'
export const GANTIPHONE = 'v2/ganti_hp'
export const GANTIBAHASA = 'change_language'
export const TRACEFITUR = 'change_status'
export const LOGIN_KARYAWAN = 'v2/login_karyawan'
export const LOGIN_NON_KARYAWAN = 'v2/login_non_karyawan'
export const SUB_NON_KARYAWAN = 'sub_category'
export const UPDATE_VERSION = 'change_version'
export const RECORD = 'add_record'