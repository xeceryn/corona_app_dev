import {traceFiturAPI} from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
export const TRACEFITUR_OK = 'TRACEFITUR_OK';
export const TRACEFITUR_FAIL = 'TRACEFITUR_FAIL';

export const requestTraceFitur = status => dispatch => {
  const payload = {
    status: status  ? 1 : 0,
  };
  // console.log('[trace payload]', payload);
  traceFiturAPI(payload)
    .then(async ({data}) => {
      data = {
        ...data,
        status_tracing: status ? 1 : 0,
      };
      dispatch(traceFiturOK(data));
      console.log('[traceOk]', data)
      console.log('[traceOk][payload]', payload)
      console.log('[traceOk][status]', status)
    })
    .catch(e => {
      dispatch(traceFiturFAIL(e));
      // console.log('[traceFail]', e)
    });
};

const traceFiturOK = data => {
  return {
    type: TRACEFITUR_OK,
    data,
  };
};

const traceFiturFAIL = data => {
  return {
    type: TRACEFITUR_FAIL,
    data,
  };
};
