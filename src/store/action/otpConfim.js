import AsyncStorage from '@react-native-community/async-storage';
import {sendOtp} from '../../api';

export const OTP_OK = 'SEND_OK';
export const OTP_FAIL = 'SEND_FAIL';

export const sendOTP = (otp) => async (dispatch) => {
  const dataPhone = await AsyncStorage.getItem('userPhone');
  // console.log('sendOTP otp ', otp)
  const fcm_Token = await AsyncStorage.getItem('tokenFCM')
  const lang = await AsyncStorage.getItem('language')
  // console.log('sendOTP dataPhone ', dataPhone)
  if (dataPhone) {
    const payload = {
      phone: dataPhone,
      otp,
      fcm_token: fcm_Token,
      lang: lang == 'id' ? 'ID' : 'EN'
    };
    // console.log('sendOTP payload ', payload)
    sendOtp(payload)
      .then(({data}) => {
        // console.log(' sendOtp ', data);
        dispatch(respOtpOK(data));
      })
      .catch((error) => {
        dispatch(respOtpFail(error));
      });
  }
};

const respOtpOK = (data) => {
  return {
    type: OTP_OK,
    data,
  };
};

const respOtpFail = (error) => {
  return {
    type: OTP_FAIL,
    error,
  };
};
