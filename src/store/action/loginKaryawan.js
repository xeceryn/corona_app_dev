import {Platform} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import {loginKaryawan} from '../../api/'
export const LOGIN_KARYAWAN_OK = 'LOGIN_KARYAWAN_OK'
export const LOGIN_KARYAWAN_FAIL = 'LOGIN_KARYAWAN_FAIL'
export const goLoginKaryawan = (id, code, callingCode, phone, token, language) => (dispatch) => {
    const payload = {
        employee_code: code,
        perusahaan: id,
        phone: `+${callingCode}${phone}`,
        fcm_token: token,
        lang: language,
        device: `${Platform.OS.toUpperCase()}-${DeviceInfo.getModel()}`,
        version: DeviceInfo.getVersion()
    }
    // console.log('[payload log karyawan]', payload)
    loginKaryawan(payload).then(({data}) => {
        dispatch(logKaryawanOk(data))
        // console.log('[data resp]', data)
    }).catch(err => {
        dispatch(logKaryawanFail(err))
        // console.log('[err resp]', err)
    })
}
const logKaryawanOk = (data) => {
    return {
        type: LOGIN_KARYAWAN_OK,
        data
    }
}
const logKaryawanFail = (data) => {
    return {
        type: LOGIN_KARYAWAN_FAIL,
        data
    }
}