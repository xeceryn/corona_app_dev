import {informasi} from '../../api';

export const INFORMASI_OK = 'INFORMASI_OK';
export const INFORMASI_FAIL = 'INFORMASI_FAIL';

export const getInformasi = () => dispatch => {
  //   console.log('action Informasi')
  informasi()
    .then(({data}) => {
      dispatch(respInformasiOk(data));
      //   alert(JSON.stringify(data));
      // console.log('RESP Informasi', data)
    })
    .catch(error => {
      // console.log('ERROR ', error)
      dispatch(respInformasiFail(error));
    });
};
const respInformasiOk = data => {
  return {
    type: INFORMASI_OK,
    data,
  };
};
const respInformasiFail = error => {
  return {
    type: INFORMASI_FAIL,
    error,
  };
};
