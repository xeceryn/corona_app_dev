import {Platform} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import {loginNonKaryawan} from '../../api'
export const LOGIN_NON_OK = 'LOGIN_NON_OK'
export const LOGIN_NON_FAIL = 'LOGIN_NON_FAIL'
export const goLoginVisitor = (name, nik, phone, callingCode, company, sub, token, language) => (dispatch) => {
    const payload = {
        nama: name,
        no_hp: `+${callingCode}${phone}`,
        perusahaan: company,
        nik: nik,
        sub_category: sub,
        fcm_token: token,
        lang: language,
        device: `${Platform.OS.toUpperCase()}-${DeviceInfo.getModel()}`,
        version: DeviceInfo.getVersion()
    }
    // console.log('[visitor payload]', payload)
    loginNonKaryawan(payload).then(({data}) => {
        dispatch(logNonOk(data))
    }).catch(err => {
        dispatch(logNonFail(err))
    })
}
const logNonOk = (data) => {
    return {
        type: LOGIN_NON_OK,
        data
    }
}
const logNonFail = (data) => {
    return {
        type: LOGIN_NON_FAIL,
        data
    }
}