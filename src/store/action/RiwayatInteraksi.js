import {riwayatInteraksi} from '../../api';

export const RIWAYAT_OK = 'RIWAYAT_OK';
export const RIWAYAT_FAIL = 'RIWAYAT_FAIL';

export const getRiwayatInterkasi = () => dispatch => {
  // console.log('action Interaksi');
  riwayatInteraksi()
    .then(({data}) => {
      dispatch(respRiwayatOk(data));
      // console.log('RESP action Interaksi', data);
    })
    .catch(error => {
      // console.log('ERROR ', error);
      dispatch(respRiwayatFail(error));
    });
};
const respRiwayatOk = data => {
  return {
    type: RIWAYAT_OK,
    data,
  };
};
const respRiwayatFail = error => {
  return {
    type: RIWAYAT_FAIL,
    error,
  };
};
