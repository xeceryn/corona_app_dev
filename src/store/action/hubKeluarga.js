import {hubungan} from '../../api';

export const KELUARGA_OK = 'KELUARGA_OK';
export const KELUARGA_FAIL = 'KELUARGA_FAIL';

export const getHubungan= () => dispatch => {

  // console.log('action Tracking HOME');
  hubungan()
    .then(({data}) => {
      dispatch(respKeluargaOk(data));
      // console.log('RESP Tracing', data);
    })
    .catch(error => {
      // console.log('ERROR ', error);
      dispatch(respKeluargaFail(error));
    });
};
const respKeluargaOk = data => {
  return {
    type: KELUARGA_OK,
    data,
  };
};
const respKeluargaFail = error => {
  return {
    type: KELUARGA_FAIL,
    error,
  };
};
