import {updateApp} from '../../api';
export const UPDATE_OK = 'UPDATE_OK';
export const UPDATE_FAIL = 'UPDATE_FAIL';

export const getUpdateApp = () => dispatch => {
  
  updateApp()
    .then(({data}) => {
      dispatch(respUpdateOk(data));
      // console.log('action getUpdateApp HOME ',data);
    })
    .catch(error => {
      // console.log('ERROR ', error);
      dispatch(respUpdateFail(error));
    });
};
const respUpdateOk = data => {
  return {
    type: UPDATE_OK,
    data,
  };
};
const respUpdateFail = error => {
  return {
    type: UPDATE_FAIL,
    error,
  };
};
