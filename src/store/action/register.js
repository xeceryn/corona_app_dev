import DeviceInfo from 'react-native-device-info'
import AsyncStorage from '@react-native-community/async-storage'
import { register } from '../../api'
export const REGIST_OK = 'REGIST_OK'
export const REGIST_FAIL = 'REGIST_FAIL'

export const requestRegist = (token, nik, nama, otp) => async (dispatch) => {
    const dataPhone = await AsyncStorage.getItem('userPhone')
    if(dataPhone) {
        // const dataObj = JSON.parse(dataPhone)
        const payload = {
            phone: dataPhone,
            fcm_token: token,
            uuid: DeviceInfo.getUniqueId(),
            nik,
            nama,
            otp
        }

        console.log('PAYLOAD requestRegist ', payload)
        register(payload).then(({data}) => {
            dispatch(respRegistOk(data))
            console.log('respRegistOk ',  data)
        }).catch((error) => {
            dispatch(respRegistErr(error))
        })
    }
}
const respRegistOk = (data) => {
    return {
        type: REGIST_OK,
        data
    }
}
const respRegistErr = (data) => {
    return {
        type: REGIST_FAIL,
        data
    }
}