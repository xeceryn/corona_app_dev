import {regKaryawan} from '../../api';
export const EMP_OK = 'EMP_OK';
export const EMP_FAIL = 'EMP_FAIL';
export const requestSendKaryawan = (id, code) => dispatch => {
  const payload = {
    perusahaan: id,
    employee_code: code,
  };
  // alert(JSON.stringify(payload));
  // console.log('payload reg OTP employee ', payload)
  regKaryawan(payload)
    .then(({data}) => {
      dispatch(respRegOk(data));
      console.log('regKaryawan ', data);
    })
    .catch(e => {
      dispatch(respRegFail(e));
      console.log('Eror regKaryawan ', e);
    });
};
const respRegOk = data => {
  return {
    type: EMP_OK,
    data,
  };
};
const respRegFail = data => {
  return {
    type: EMP_FAIL,
    data,
  };
};
