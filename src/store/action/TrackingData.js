import {trackingData} from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
export const TARCE_OK = 'TRACE_OK';
export const TRACE_FAIL = 'TRACE_FAIL';

export const getTrackingData = (payload) => dispatch => {
  // console.log('action Tracking HOME');
  trackingData(payload)
    .then(({data}) => {
      dispatch(respTrackingOk(data));
        console.log('[tracing] ', data);
      // try {
      //   AsyncStorage.setItem('LastData', data.bersinggungan.daily.toString());
      // } catch (error) {
      // }
    })
    .catch(error => {
      // console.log('ERROR ', error);
      dispatch(respTrackingFail(error));
    });
};
const respTrackingOk = data => {
  return {
    type: TARCE_OK,
    data,
  };
};
const respTrackingFail = error => {
  return {
    type: TRACE_FAIL,
    error,
  };
};
