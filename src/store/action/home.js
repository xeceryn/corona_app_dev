import {tracking} from '../../api';
export const HOME_OK = 'HOME_OK';
export const HOME_FAIL = 'HOME_FAIL';
export const HOME_LOAD = 'HOME_LOAD'

export const getTracking = payload => async dispatch => {
  dispatch(loadingTracing(true))
  tracking(payload)
    .then(async ({data}) => {
      await setTimeout(() => {
        dispatch(respTrackingOk(data));
      dispatch(loadingTracing(false))
      // console.log('Resp action Tracking HOME ', data);
      }, 5000)
    })
    .catch(error => {
      // console.log('ERROR action Tracing ', error);
      dispatch(respTrackingFail(error));
      dispatch(loadingTracing(false))
    });
};

const respTrackingOk = data => {
  return {
    type: HOME_OK,
    data,
  };
};

const respTrackingFail = error => {
  return {
    type: HOME_FAIL,
    error,
  };
};
const loadingTracing = (data) => {
  return {
    type: HOME_LOAD,
    data
  }
}
