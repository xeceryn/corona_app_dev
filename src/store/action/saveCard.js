export const CARD_OK = 'CARD_OK';
export const CARD_FAIL = 'CARD_FAIL';

export const saveCardAction = status => async dispatch => {
  try {
    dispatch(respSaveOK(status));
  } catch (error) {
    dispatch(respSaveFail(error));
  }
};
const respSaveOK = data => {
  return {
    type: CARD_OK,
    data,
  };
};
const respSaveFail = error => {
  return {
    type: CARD_FAIL,
    error,
  };
};
