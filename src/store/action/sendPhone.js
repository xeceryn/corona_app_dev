import AsyncStorage from '@react-native-community/async-storage';
import {sendPhone} from '../../api';

export const SEND_OK = 'SEND_OK';
export const SEND_FAIL = 'SEND_FAIL';

export const requestSendPhone = (phone, fcm_token) => async (dispatch) => {
  const payload = {
    phone,
    fcm_token
  };
  console.log('action requestSendPhone ', payload);
  sendPhone(payload)
    .then(({data}) => {
      dispatch(respSendOK(data));
      console.log('sendPhone Visitor ', data);
    })
    .catch((error) => {
      dispatch(respSendFail(error));
      console.log('sendPhone Visitor Error ', error);
    });
};
const respSendOK = (data) => {
  return {
    type: SEND_OK,
    data,
  };
};
const respSendFail = (error) => {
  return {
    type: SEND_FAIL,
    error,
  };
};
