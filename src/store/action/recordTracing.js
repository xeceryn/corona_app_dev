import { addRecord } from '../../api'
export const ADD_RECORD = 'add_record'
export const RECORD_LOAD = 'record_load'
export const recordTracing = (payload) => dispatch => {
    addRecord(payload)
        .then( async ({data}) => {
            const timer = await setTimeout(() => {
                dispatch(respRecordTracing(data))
                console.log('[then after 1 hour]', data)
            }, 3600 * 1000 )
            return () => clearTimeout(timer)
        })
        .catch(error => {
            dispatch(respRecordTracing(error))
            console.log('[catch after 1 hour]', data)
        })
}
const respRecordTracing = (data) => {
    return { type: ADD_RECORD, data }
}