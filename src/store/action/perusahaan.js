import {perusahaan} from '../../api';

export const PERUSAHAAN_OK = 'PERUSAHAAN_OK';
export const PERUSAHAAN_FAIL = 'PERUSAHAAN_FAIL';

export const getPerusahaan= () => dispatch => {

  // console.log('action Tracking HOME');
  perusahaan()
    .then(({data}) => {
      dispatch(respPerusahaanOk(data));
      // console.log('RESP Tracing', data);
    })
    .catch(error => {
      // console.log('ERROR ', error);
      dispatch(respPerusahaanFail(error));
    });
};
const respPerusahaanOk = data => {
  return {
    type: PERUSAHAAN_OK,
    data,
  };
};
const respPerusahaanFail = error => {
  return {
    type: PERUSAHAAN_FAIL,
    error,
  };
};
