import { subNonKaryawan } from '../../api'
export const SUB_OK = 'SUB_OK'
export const SUB_FAIL = 'SUB_FAIL'

export const getSubCategory = () => (dispatch) => {
    subNonKaryawan().then(({data}) => {
        // console.log('[data sub]', data)
        dispatch(subOk(data))
    }).catch(err => {
        // console.log('[data sub err]', err)
        dispatch(subFail(err))
    })
}
const subOk = (data) => {
    return {
        type: SUB_OK,
        data
    }
}
const subFail = (data) => {
    return {
        type: SUB_FAIL,
        data
    }
}