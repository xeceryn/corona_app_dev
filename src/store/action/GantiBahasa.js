import {gantiBahasa} from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
export const BAHASA_OK = 'BAHASA_OK';
export const BAHASA_FAIL = 'BAHASA_FAIL';

export const requestGantiBahasa = bahasa => dispatch => {
  //dispatch(gantiLang(bahasa));
  const payload = {
    language: bahasa == 'id' ? 'ID' : 'EN',
  };
  gantiBahasa(payload)
    .then(async ({data}) => {
      dispatch(gantiLangOK(bahasa));
      console.log('ganti Bahasa  ', data.token);
      // let prevToken = await AsyncStorage.getItem('token');
      await AsyncStorage.setItem('token', data.token);
      // let newToken = await AsyncStorage.getItem('token');
      // if (prevToken != newToken) {
      //   alert('token berubah');
      // }
    })
    .catch(e => {
      dispatch(gantiLangFAIL(e));
      console.log('Eror Bahasa ', e);
    });
};

const gantiLangOK = data => {
  return {
    type: BAHASA_OK,
    data,
  };
};

const gantiLangFAIL = data => {
  return {
    type: BAHASA_FAIL,
    data,
  };
};
