import DeviceInfo from 'react-native-device-info'
import { updateVersion } from '../../api'
export const UPDATEVER_OK = 'UPDATE_OK'
export const UPDATEVER_FAIL = 'UPDATE_FAIL'

export const setVersion = () => (dispatch) => {
    const payload = {
        version: DeviceInfo.getVersion()
    }
    // console.log('[setVersion][payload]', payload)
    updateVersion(payload).then(({data}) => {
        dispatch(updateverOk(data))
        // console.log('[setVersion]', data)
    }).catch((err) => {
        dispatch(updateverFail(err))
    })
}
const updateverOk = (data) => {
    return {
        type: UPDATEVER_OK,
        data
    }
}
const updateverFail = (data) => {
    return {
        type: UPDATEVER_FAIL,
        data
    }
}