import {gantiPhone} from '../../api';
export const GANTI_OK = 'GANTI_OK';
export const GANTI_FAIL = 'GANTI_FAIL';
export const requestGantiPhone = (phone, callingCode) => dispatch => {
  const payload = {
    phone: `+${callingCode}${phone}`,
  };

  console.log('[phone payload]', payload)
  gantiPhone(payload)
    .then(({data}) => {
      dispatch(respRegOk(data));
      // console.log('ganti Phone Karyawan ', data);
    })
    .catch(e => {
      dispatch(respRegFail(e));
      // console.log('Eror ganti Phone Karyawan ', e);
    });
};
const respRegOk = data => {
  return {
    type: GANTI_OK,
    data,
  };
};
const respRegFail = data => {
  return {
    type: GANTI_FAIL,
    data,
  };
};
