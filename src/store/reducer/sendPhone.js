import * as types from '../action/sendPhone'
const initialState = {
    respSendOk: null,
    respSendFail: null
}
const sendPhone = (state = initialState, action) => {
    switch(action.type) {
        case types.SEND_OK:
            return {
                ...state,
                respSendOk: action.data
            }
        case types.SEND_FAIL:
            return {
                ...state,
                respSendFail: action.error
            }
        default:
            return state
    }
}
export default sendPhone