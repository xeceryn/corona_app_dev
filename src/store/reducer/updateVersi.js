import * as types from '../action/updateVersi'
const initialState = {
    updateverOk: null,
    updateverFail: null
}
const setVersion = (state = initialState, action) => {
    switch(action.type) {
        case types.UPDATEVER_OK:
            return {
                ...state,
                updateverOk: action.data
            }
        case types.UPDATEVER_FAIL:
            return {
                ...state,
                updateverFail: action.data
            }
        default:
            return state
    }
}
export default setVersion