import {combineReducers} from 'redux';
import { reducer as network } from 'react-native-offline'
import sendPhone from './sendPhone';
import register from './register';
import regKaryawan from './regKaryawan';
import informasi from './informasi';
import home from './home';
import perusahaan from './perusahaan';
import hubKeluarga from './hubKeluarga';
import trackingData from './TrackingData';
import otpConfirm from './otpConfirm';
import saveCard from './saveCard';
import updateApp from './updateApp';
import RiwayatInteraksi from './RiwayatInteraksi';
import gantiPhone from './gantiPhone'
import GantiBahasa from './GantiBahasa'
import TraceFitur from './TraceFitur'
import loginKaryawan from './loginKaryawan'
import subCategory from './subCategory'
import loginVisitor from './loginVisitor'
import setVersion from './updateVersi'

const rootReducer = combineReducers({
  sendPhone,
  register,
  regKaryawan,
  informasi,
  home,
  perusahaan,
  hubKeluarga,
  trackingData,
  otpConfirm,
  saveCard,
  updateApp,
  RiwayatInteraksi,
  gantiPhone,
  GantiBahasa,
  TraceFitur,
  loginKaryawan,
  subCategory,
  loginVisitor,
  setVersion,
  network
});
export default rootReducer;
