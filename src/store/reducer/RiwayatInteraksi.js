import * as types from '../action/RiwayatInteraksi'
const initialState = {
    resRiwayatOk: null,
    resRiwayatFail: null
}
const RiwayatInteraksi = (state = initialState, action) => {
    switch(action.type) {
        case types.RIWAYAT_OK:
            // console.log('reducerRIWAYAT ', action.data)
            return {
                ...state,
                resRiwayatOk: action.data
            }
        case types.RIWAYAT_FAIL:
            return {
                ...state,
                resRiwayatFail: action.error
            }
        default:
            return state
    }
}
export default RiwayatInteraksi