import * as types from '../action/updateApp'
const initialState = {
    respUpdateOk: null,
    respUpdateFail: null
}
const updateApp = (state = initialState, action) => {
    switch(action.type) {
        case types.UPDATE_OK:
            // console.log('reducer Home ', action.data)
            return {
                ...state,
                respUpdateOk: action.data
            }
        case types.UPDATE_FAIL:
            // console.log('reducer Home ERROR ', action.data)
            return {
                ...state,
                respUpdateFail: action.error
            }
        default:
            return state
    }
}
export default updateApp