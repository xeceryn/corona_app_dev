import * as types from '../action/informasi'
const initialState = {
    resInformasiOk: null,
    resInformasiFail: null
}
const informasi = (state = initialState, action) => {
    switch(action.type) {
        case types.INFORMASI_OK:
            // console.log('reducer ', action.data)
            return {
                ...state,
                resInformasiOk: action.data
            }
        case types.INFORMASI_FAIL:
            return {
                ...state,
                resInformasiFail: action.error
            }
        default:
            return state
    }
}
export default informasi