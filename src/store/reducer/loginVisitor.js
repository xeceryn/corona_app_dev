import * as types from '../action/loginVisitor'
const initialState = {
    logNonOk: null,
    logNonFail: null
}
const loginVisitor = (state = initialState, action) => {
    switch(action.type) {
        case types.LOGIN_NON_OK:
            return {
                ...state,
                logNonOk: action.data
            }
        case types.LOGIN_NON_FAIL:
            return {
                ...state,
                logNonFail: action.data
            }
        default:
            return state
    }
}
export default loginVisitor