import * as types from '../action/gantiPhone';
const initialState = {
  respGantiOk: null,
  respGantiFail: null,
};
const gantiPhone = (state = initialState, action) => {
  switch (action.type) {
    case types.GANTI_OK:
    //   console.log('Reducer OK: ', action.data);
      return {
        ...state,
        respGantiOk: action.data,
      };
    case types.GANTI_FAIL:
    //   console.log('Reducer FAIL: ', action.data);
      return {
        ...state,
        respGantiFail: action.data,
      };
    default:
      return state;
  }
};
export default gantiPhone;
