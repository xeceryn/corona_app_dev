import * as types from '../action/regKaryawan'
const initialState = {
    respRegOk: null,
    respRegFail: null
}
const regKaryawan = (state = initialState, action) => {
    switch(action.type) {
        case types.EMP_OK:
            return {
                ...state,
                respRegOk: action.data
            }
        case types.EMP_FAIL:
            return {
                ...state,
                respRegFail: action.data
            }
        default:
            return state
    }
}
export default regKaryawan