import * as types from '../action/register'
const initialState = {
    respRegistOk: null,
    respRegistFail: null
}
const register = (state = initialState, action) => {
    switch(action.type) {
        case types.REGIST_OK:
            return {
                ...state,
                respRegistOk: action.data
            }
        case types.REGIST_FAIL:
            return {
                ...state,
                respRegistFail: action.data
            }
        default:
            return state
    }
}
export default register