import * as types from '../action/saveCard'
const initialState = {
    respSaveOk: null,
    respSaveFail: null
}
const saveCard = (state = initialState, action) => {
    switch(action.type) {
        case types.SEND_OK:
            return {
                ...state,
                respSaveOk: action.data
            }
        case types.SEND_FAIL:
            return {
                ...state,
                respSaveFail: action.error
            }
        default:
            return state
    }
}
export default saveCard