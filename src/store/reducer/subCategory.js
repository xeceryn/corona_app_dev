import * as types from '../action/subCategory'
const initialState = {
    subOk: null,
    subFail: null
}
const subCategory = (state = initialState, action) => {
    switch(action.type) {
        case types.SUB_OK:
            return {
                ...state,
                subOk: action.data
            }
        case types.SUB_FAIL:
            return {
                ...state,
                subFail: action.data
            }
        default:
            return state
    }
}
export default subCategory