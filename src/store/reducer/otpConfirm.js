import * as types from '../action/otpConfim'
const initialState = {
    respOtpOk: null,
    respOtpFail: null
}
const sendOtp = (state = initialState, action) => {
    switch(action.type) {
        case types.OTP_OK:
            return {
                ...state,
                respOtpOk: action.data
            }
        case types.OTP_FAIL:
            return {
                ...state,
                respOtpFail: action.error
            }
        default:
            return state
    }
}
export default sendOtp