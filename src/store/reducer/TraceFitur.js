import * as types from '../action/traceFitur';
const initialState = {
  traceFiturOk: null,
  traceFiturFail: null,
};
const traceFitur = (state = initialState, action) => {
  switch (action.type) {
    case types.TRACEFITUR_OK:
    // alert(JSON.stringify(action.data))
      return {
        ...state,

        traceFiturOk: action.data,
      };
    case types.TRACEFITUR_FAIL:
      return {
        ...state,
        traceFiturFail: action.data,
      };
    default:
      return state;
  }
};
export default traceFitur;
