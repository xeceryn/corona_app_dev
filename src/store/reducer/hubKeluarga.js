import * as types from '../action/hubKeluarga'
const initialState = {
    reshubKeluargaOk: null,
    reshubKeluargaFail: null
}
const hubKeluarga = (state = initialState, action) => {
    switch(action.type) {
        case types.KELUARGA_OK:
            // console.log('reducerHubungan ', action.data.data[0])
            return {
                ...state,
                reshubKeluargaOk: action.data
            }
        case types.KELUARGA_FAIL:
            return {
                ...state,
                reshubKeluargaFail: action.error
            }
        default:
            return state
    }
}
export default hubKeluarga