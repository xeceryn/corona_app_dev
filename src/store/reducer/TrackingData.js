import * as types from '../action/TrackingData'
const initialState = {
    resTraceOk: null,
    resTraceFail: null
}
const TracingData = (state = initialState, action) => {
    switch(action.type) {
        case types.TARCE_OK:
            // console.log('reducer Home ', action.data)
            return {
                ...state,
                resTraceOk: action.data
            }
        case types.TRACE_FAIL:
            // console.log('reducer Home ERROR ', action.data)
            return {
                ...state,
                resTraceFail: action.error
            }
        default:
            return state
    }
}
export default TracingData