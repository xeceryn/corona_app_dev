import * as types from '../action/perusahaan'
const initialState = {
    resPerusahaanOk: null,
    resPerusahaanFail: null
}
const perusahaan = (state = initialState, action) => {
    switch(action.type) {
        case types.PERUSAHAAN_OK:
            // console.log('reducerPerusahaan ', action.data)
            return {
                ...state,
                resPerusahaanOk: action.data
            }
        case types.PERUSAHAAN_FAIL:
            return {
                ...state,
                resPerusahaanFail: action.error
            }
        default:
            return state
    }
}
export default perusahaan