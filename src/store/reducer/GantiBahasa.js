import * as types from '../action/GantiBahasa';
const initialState = {
  gantiBahasaOk: null,
  gantiBahasaFail: null,
};
const gantiBahasa = (state = initialState, action) => {
  switch (action.type) {
    case types.BAHASA_OK:
      return {
        ...state,

        gantiBahasaOk: action.data,
      };
    case types.BAHASA_FAIL:
      return {
        ...state,
        gantiBahasaFail: action.data,
      };
    default:
      return state;
  }
};
export default gantiBahasa;
