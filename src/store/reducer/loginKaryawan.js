import * as types from '../action/loginKaryawan'
const initialState = {
    logKaryawanOk: null,
    logKaryawanFail: null
}
const loginKaryawan = (state = initialState, action) => {
    switch(action.type) {
        case types.LOGIN_KARYAWAN_OK:
            return {
                ...state,
                logKaryawanOk: action.data
            }
        case types.LOGIN_KARYAWAN_FAIL:
            return {
                ...state,
                logKaryawanFail: action.data
            }
        default:
            return state
    }
}
export default loginKaryawan