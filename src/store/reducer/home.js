import * as types from '../action/home'
const initialState = {
    resHomeOk: null,
    resHomeFail: null,
    loading: false
}
const home = (state = initialState, action) => {
    switch(action.type) {
        case types.HOME_OK:
            // console.log('reducer Home ', action.data)
            return {
                ...state,
                resHomeOk: action.data
            }
        case types.HOME_FAIL:
            // console.log('reducer Home ERROR ', action.data)
            return {
                ...state,
                resHomeFail: action.error
            }
        case types.HOME_LOAD:
            // console.log('[LOAD]', action.data)
            return {
                ...state,
                loading: action.data
            }
        default:
            return state
    }
}
export default home