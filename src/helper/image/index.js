const SplashImage = {
  LogoApp: require('../../assets/image/splashscreen/logoApp.png'),
  LogoPerusahaan: require('../../assets/image/splashscreen/logoPerusahaan.png'),
};
const IntroImage = {
  IntroIlust: require('../../assets/image/introduction/introIlustrasi.png'),
};
const RoleImage = {
  Karwayan: require('../../assets/image/roleModal/karyawan.png'),
  Visitor: require('../../assets/image/roleModal/visitor.png'),
};
const LoginImage = {
  LoginIlust: require('../../assets/image/login/loginIlustrasi.png'),
};
const PermissionImage = {
  BT_Perm: require('../../assets/image/permissions/bluetooth.png'),
  Modal_Perm: require('../../assets/image/permissions/Modal_Perm.png'),
  PermOk: require('../../assets/image/permissions/permOk.png'),
};
const HomeImage = {
  Home_BT: require('../../assets/image/home_bt.png'),
  Home_BT2: require('../../assets/image/HOME_BT2.png'),
  Home_INF: require('../../assets/image/HOME_INF.png'),
  Home_STAY: require('../../assets/image/HOME_STAY.png'),
  Home_List: require('../../assets/image/iconList.png'),
};
const EmergencyImage = {
  EM_IC: require('../../assets/image/emergency_icon.png'),
  Call: require('../../assets/image/emergency/call.png'),
  Status: require('../../assets/image/emergency/laporkanStatus.png'),
  Umum: require('../../assets/image/emergency/layananUmum.png'),
};
const BottomButton = {
  Dashboard: require('../../assets/image/tabIcon/home.png'),
  Emergency: require('../../assets/image/tabIcon/emergency.png'),
  UploadData: require('../../assets/image/tabIcon/uploadData.png'),
  Information: require('../../assets/image/tabIcon/information.png'),
};
const ArrowIcon = {
  UP: require('../../assets/image/arrow/arrowUp.png'),
  DOWN: require('../../assets/image/arrow/arrowDown.png'),
  RIGHT: require('../../assets/image/arrow/arrowRight.png'),
};
const Flag = {
  IND: require('../../assets/image/flag/IND.png'),
  ENG: require('../../assets/image/flag/ENG.png'),
};
const Settings = {
  logout: require('../../assets/image/logout/ic_logout.png')
}
const Animasi = {
  tracing: require('../../assets/image/gif/tracing_load.gif')
}
export {
  SplashImage,
  IntroImage,
  RoleImage,
  LoginImage,
  PermissionImage,
  HomeImage,
  EmergencyImage,
  BottomButton,
  ArrowIcon,
  Flag,
  Settings,
  Animasi
};
