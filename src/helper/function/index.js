import BleManager from 'react-native-ble-manager'

export function BluetoothPermission() {
    BleManager.start({ showAlert: false }).then(() => {
        BleManager.enableBluetooth().then(() => {
            console.log('[enabled ok]')
        }).catch(() => {
            console.log('[enabled fail]')
        })
    })
}

export const medianAltitude = (value) => {
    switch(value) {
        case value == 20:
            return 9
        case value = 21:
            return 9.5
        case value = 22:
            return 10
        case value = 23:
            return 10.5
        case value = 24:
            return 11
        case value = 25:
            return 11.5
        case value = 26:
            return 12
        case value = 27:
            return 12.5
        case value = 28:
            return 13
        case value = 29:
            return 13.5
        case value = 30:
            return 14
        case value = 31:
            return 14.5
        case value = 32:
            return 15
        case value = 33:
            return 15.5
        case value = 34:
            return 16
        case value = 35:
            return 16.5
        case value = 36:
            return 17
        case value = 37:
            return 17.5
        case value = 38:
            return 18
        case value = 39:
            return 18.5
        case value = 40:
            return 19
        case value = 41:
            return 19.5
        case value = 42:
            return 20
        case value = 43:
            return 20.5
        case value = 44:
            return 21
        case value = 45:
            return 21.5
        case value = 46:
            return 22
        case value = 47:
            return 22.5
        case value = 48:
            return 23
        case value = 49:
            return 23.5
        case value = 50:
            return 24
        case value = 51:
            return 24.5
        case value = 52:
            return 25
        case value = 53:
            return 25.5
        case value = 54:
            return 26
        case value = 55:
            return 26.5
        default:
            return value
    }
}