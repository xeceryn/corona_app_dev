import PushNotification from 'react-native-push-notification';
import PushNotificatioIOS from '@react-native-community/push-notification-ios';
import {Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class NotificationManager {
  configure = (onRegister, onNotification, onOpenNotification, senderID) => {
  // configure = (onRegister, onNotification, onOpenNotification) => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        onRegister(token);
        // console.log('[NotificationManager] onRegister TOKEN:', token);
        AsyncStorage.setItem('tokenFCM', token.token);
      },
      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
        // console.log(
        //   '[NotificationManager] onNotification NOTIFICATION:',
        //   notification,
        // );

        if (Platform.OS == 'ios') {
          if (notification.data.openedInForeground) {
            notification.userInteraction = true;
          }
        } else {
          notification.userInteraction = true;
        }

        if (notification.userInteraction) {
          onOpenNotification(notification);
        } else {
          onNotification(notification);
        }

        //Only Call back if not from foreground
        if (Platform.OS == 'ios') {
          if (!notification.data.openedInForeground) {
            notification.finish('backgroundFetchResultNoData');
          }
        } else {
          notification.finish('backgroundFetchResultNoData');
        }
        // process the notification

        // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
      senderID: senderID,
    });
  };

  _buildAndroidNotification = (id, title, message, data = {}, options = {}) => {
    return {
      id: id,
      autoCancel: true,
      largeIcon: options.largeIcon || 'ic_launcher',
      smallIcon: options.smallIcon || 'ic_launcher',
      bigText: message || '',
      subText: title || '',
      vibrate: options.vibrate || false,
      vibration: options.vibration || 300,
      priority: options.priority || 'high',
      importance: options.importance || 'high',
      data: data,
    };
  };

  _buildIOSNotification = (id, title, message, data = {}, options = {}) => {
    return {
      alertAction: options.alertAction || 'view',
      category: options.category || '',
      userInfo: {
        id: id,
        item: data,
      },
    };
  };

  showNotification = (id, title, message, data = {}, options = {}) => {
    PushNotification.localNotification({
      // Android only Properties
      ...this._buildAndroidNotification(id, title, message, data, options),
      // IOS Only Properties
      ...this._buildIOSNotification(id, title, message, data, options),
      // IOS and Android properties
      title: title || '',
      message: message || '',
      playSound: options.playsound || false,
      soundName: options.soundName || 'default',
      userInteraction: false, //if the notification was opened by the user
    });
  };

  cancelAllNotification = () => {
    if (Platform.OS === 'ios') {
      PushNotificatioIOS.removeAllDeliveredNotifications();
    } else {
      PushNotification.cancelAllLocalNotifications();
    }
  };

  unregister = () => {
    PushNotification.unregister()
  };
}

export const notificationManager = new NotificationManager();
