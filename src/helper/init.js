import {Dimensions} from 'react-native';

export const getHeight = size => {
  return (Dimensions.get('window').height * size) / 100;
};

export const getWidth = size => {
  return (Dimensions.get('window').width * size) / 100;
};

export const formatPhoneNumber = str => {
  //Filter only numbers from the input
  let cleaned = ('' + str).replace(/\D/g, '');

  //   //Check if the input is of correct
  if (cleaned) {
    if (cleaned.length == 14) {
      return (
        '+' +
        cleaned.substring(0, 2) +
        ' ' +
        cleaned.substring(2, 6) +
        '-' +
        cleaned.substring(6, 10) +
        '-' +
        cleaned.substring(10, 14)
      )
    } else if (cleaned.length == 13) {
      return (
        '+' +
        cleaned.substring(0, 2) +
        ' ' +
        cleaned.substring(2, 5) +
        '-' +
        cleaned.substring(5, 9) +
        '-' +
        cleaned.substring(9, 13)
      );
    } else if (cleaned.length == 12) {
      return (
        '+' +
        cleaned.substring(0, 2) +
        ' ' +
        cleaned.substring(2, 5) +
        '-' +
        cleaned.substring(5, 8) +
        '-' +
        cleaned.substring(8, 12)
      );
    } else if (cleaned.length == 11) {
      return (
        '+' +
        cleaned.substring(0, 2) +
        ' ' +
        cleaned.substring(2, 5) +
        '-' +
        cleaned.substring(5, 8) +
        '-' +
        cleaned.substring(8, 11)
      );
    }
  } else {
    return str;
  }
};
