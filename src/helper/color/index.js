export const colors = {
    white: '#FFFFFF',
    red: '#E2374F',
    charcoal: '#434343',
    night_rider: '#2E2E2E',
    shamerock: '#3FD081',
    dim_gray: '#707070',
    jacksons_purple: '#222A83'
}