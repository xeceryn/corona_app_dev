import axios from 'axios'
import { 
    BASE_URL,
    SEND_PHONE,
    SEND_OTP,
    REGISTER,
    REG_EMPLOYEE,
    INFORMASI,
    TRACKING,
    PERUSAHAAN,
    HUBUNGAN,
    TRACKINGDATA,
    UPDATEAPP,
    RIWAYATINTERAKSI,
    GANTIPHONE,
    GANTIBAHASA,
    TRACEFITUR,
    LOGIN_KARYAWAN,
    LOGIN_NON_KARYAWAN,
    SUB_NON_KARYAWAN,
    UPDATE_VERSION,
    RECORD
} from '../constants'
export const sendPhone = (payload) => axios.post(`${BASE_URL}${SEND_PHONE}`, payload)
export const sendOtp = (payload) => axios.post(`${BASE_URL}${SEND_OTP}`, payload)
export const register = (payload) => axios.post(`${BASE_URL}${REGISTER}`, payload)
export const regKaryawan = (payload) => axios.post(`${BASE_URL}${REG_EMPLOYEE}`, payload)
export const informasi = () => axios.get(`${BASE_URL}${INFORMASI}`)
export const perusahaan = () => axios.get(`${BASE_URL}${PERUSAHAAN}`)
export const tracking = (payload) => axios.post(`${BASE_URL}${TRACKING}`, payload)
export const hubungan = () => axios.get(`${BASE_URL}${HUBUNGAN}`)
export const trackingData = (payload) => axios.post(`${BASE_URL}${TRACKINGDATA}`, payload)
export const updateApp = () => axios.get(`${BASE_URL}${UPDATEAPP}`)
export const riwayatInteraksi = (payload) => axios.post(`${BASE_URL}${RIWAYATINTERAKSI}`, payload)
export const gantiPhone = (payload) => axios.post(`${BASE_URL}${GANTIPHONE}`, payload)
export const gantiBahasa = (payload) => axios.post(`${BASE_URL}${GANTIBAHASA}`, payload)
export const traceFiturAPI = (payload) => axios.post(`${BASE_URL}${TRACEFITUR}`, payload)
export const loginKaryawan = (payload) => axios.post(`${BASE_URL}${LOGIN_KARYAWAN}`, payload)
export const loginNonKaryawan = (payload) => axios.post(`${BASE_URL}${LOGIN_NON_KARYAWAN}`, payload)
export const subNonKaryawan = () => axios.get(`${BASE_URL}${SUB_NON_KARYAWAN}`)
export const updateVersion = (payload) => axios.post(`${BASE_URL}${UPDATE_VERSION}`, payload)
export const addRecord = (payload) => axios.post(`${BASE_URL}${RECORD}`, payload)
