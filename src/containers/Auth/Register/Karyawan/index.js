import React, { Component } from 'react';
import { Text, View, TextInput, Alert, StyleSheet, Keyboard } from 'react-native';
import { Picker } from '@react-native-community/picker';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import CountryPicker from 'react-native-country-picker-modal';
import countriesList from '../../../../../countries.json';
import * as RNLocalize from 'react-native-localize';
var moment = require('moment')
import Snackbar from 'react-native-snackbar'

import { colors } from '../../../../helper/color';

import RoundedButton from '../../../../components/RoundedButton';

import { getPerusahaan } from '../../../../store/action/perusahaan';
import { requestSendKaryawan } from '../../../../store/action/regKaryawan';
import { sendOTP } from '../../../../store/action/otpConfim';

import { goLoginKaryawan } from '../../../../store/action/loginKaryawan'

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyName: '',
      companyPick: '',
      noreg: '',
      TokenFCM: '',
      userName: '',
      Password: '',
      phone: '',
      language: '',
      marginConatiner: '10%',
      countryCode: 'ID',
      callingCode: '62',
      countriesList: countriesList
    };
  }
  async componentDidMount() {
    let language = await AsyncStorage.getItem('language');
    // countryCode = RNLocalize.getLocales()[0].countryCode;
    this.setState({ language });
    const { getPerusahaan } = this.props;
    getPerusahaan();
    // getFCM
    this.getTokenFCM();
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.resPerusahaanOk != this.props.resPerusahaanOk) {
      this.setState({ companyName: this.props.resPerusahaanOk.data });
    }
    if (prevProps.logKaryawanOk != this.props.logKaryawanOk) {
      if(this.props.logKaryawanOk.code === 200) {
        this.saveUserDetail(this.props.logKaryawanOk);
      } else if (this.props.logKaryawanOk.code === 400) {
        Alert.alert(
          'Around Us',
          this.props.logKaryawanOk.message,
          [
            { text: 'Ingin ganti nomor ?', onPress: () => this.changePhone(this.props.logKaryawanOk) },
            { text: 'Oke', onPress: () => this.saveUserDetail(this.props.logKaryawanOk) }
          ]
        );
      } else {
        Alert.alert(
          'Around Us',
          'Anda belum terdaftar di Perusahaan tersebut',
          [{ text: 'Oke', onPress: () => console.log('Ok pressed') }],
        );
      }
      // console.log('[login karyawan reading]', this.props.logKaryawanOk)
    }
  }

  keyboardDidShow = () => {
    console.log('Keyboard Shown');
  };

  keyboardDidHide = () => {
    console.log('Keyboard Hidden');
    this.setState({ marginConatiner: '10%' });
  };

  getTokenFCM = () => {
    AsyncStorage.getItem('tokenFCM', (error, result) => {
      if (result) {
        // alert(result)
        this.setState({
          TokenFCM: result,
        });
      }
    });
  };

  saveUserDetail = async data => {
    const dateTime = moment().format('LLLL')
    // console.log('[data save]', data)
    await AsyncStorage.setItem('token', data.token);
    await AsyncStorage.setItem('userPhone', data.profile.no_hp);
    await AsyncStorage.setItem('noReg', data.profile.employee_code);
    await AsyncStorage.setItem('perusahaan', data.profile.id);
    await AsyncStorage.setItem('lastLogin', dateTime);
    await AsyncStorage.setItem('service_id', data.profile.service_id);
    this.props.navigation.navigate('Permissions');
  };
  changePhone = async data => {
    const dateTime = moment().format('LLLL')
    // console.log('[data will change]', data)
    await AsyncStorage.setItem('token', data.token)
    await AsyncStorage.setItem('userPhone', data.profile.no_hp);
    await AsyncStorage.setItem('noReg', data.profile.employee_code);
    await AsyncStorage.setItem('perusahaan', data.profile.id);
    await AsyncStorage.setItem('lastLogin', dateTime);
    this.props.navigation.navigate('Login', {whoThis: 'employee'})
  }
  handleRegKaryawan = () => {
    const {goLoginKaryawan} = this.props;
    const { companyPick, noreg, callingCode, phone, TokenFCM, language } = this.state;
    if (companyPick && noreg && phone) {
      goLoginKaryawan(companyPick, noreg, callingCode, phone, TokenFCM, language)
    } else {
      Alert.alert(
        'Around Us',
        'Salah satu parameter kosong, isi data terlebih dahulu!',
        [{ text: 'Oke', onPress: () => console.log('Ok Pressed') }],
      );
    }
  };
  render() {
    const {
      companyName,
      language,
      marginConatiner,
      countryCode,
      callingCode,
      companyPick
    } = this.state;
    return (
      <View style={styles.container}>
        {this.props.network ? (
          Snackbar.dismiss()
        ) : (
          Snackbar.show({
            text: language == 'id' ? 'Tidak Ada Koneksi!' : 'No Internet Connection!',
            duration: Snackbar.LENGTH_INDEFINITE,
          })
        )}
        <View
          style={{
            marginHorizontal: '8%',
            marginVertical: marginConatiner,
          }}>
          <Text style={styles.otpText}>
            {language == 'id'
              ? `Login sebagai Karyawan`
              : `Login as an employee`}
          </Text>
        </View>
        <View style={styles.containerDropdown}>
          <Text style={styles.dropdownTitle}>{language == 'id' ? `Perusahaan` : `Company`}</Text>
          <Picker
            selectedValue={companyPick}
            onValueChange={itemValue => {
              this.setState({ companyPick: itemValue });
            }}
            style={styles.pickerCompany}
          >
            {companyName !== ''
              ? companyName.map((item, index) => {
                return (
                  <Picker.Item
                    key={index}
                    label={item.company_name}
                    value={item.id}
                  />
                );
              })
              : null}
          </Picker>
          <Text style={styles.dropdownTitle}>{language == 'id' ? `Noreg Karyawan` : `Employee ID`}</Text>
          <TextInput
            style={styles.dropdownInput}
            underlineColorAndroid={colors.charcoal}
            maxLength={8}
            keyboardType="number-pad"
            onChangeText={text => this.setState({ noreg: text })}
          />
          <Text style={styles.dropdownTitle}>{language == 'id' ? `Nomor Telepon` : `Phone Number`}</Text>
          <View style={styles.containerInput}>
            <CountryPicker
              countryCode={countryCode}
              withCallingCode={true}
              onSelect={item => {
                this.setState({
                  countryCode: item.cca2,
                  callingCode: item.callingCode,
                });
              }}
            />
            <Text style={styles.countryCode}>{`+${callingCode}`}</Text>
            <TextInput
              keyboardType="number-pad"
              maxLength={15}
              selectionColor={colors.charcoal}
              underlineColorAndroid={colors.charcoal}
              style={styles.inputForm}
              onChangeText={text => this.setState({ phone: text })}
            />
          </View>
        </View>
        <RoundedButton
          title={language == 'id' ? 'Lanjutkan' : 'Next'}
          onPress={() => this.handleRegKaryawan()}
          bottom={'5%'}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  resPerusahaanOk: state.perusahaan.resPerusahaanOk,
  respRegOk: state.regKaryawan.respRegOk,
  respOtpOk: state.otpConfirm.respOtpOk,
  logKaryawanOk: state.loginKaryawan.logKaryawanOk,
  network: state.network.isConnected
});
const mapDispatchToProps = dispatch => ({
  getPerusahaan: () => dispatch(getPerusahaan()),
  requestSendKaryawan: (id, code) => dispatch(requestSendKaryawan(id, code)),
  goLoginKaryawan: (id, code, callingCode, phone, token) => dispatch(goLoginKaryawan(id, code, callingCode, phone, token)),
  sendOTP: otp => dispatch(sendOTP(otp)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  containerScroll: {
    marginBottom: '15%',
  },
  otpText: {
    fontSize: 20,
    color: colors.night_rider,
    fontWeight: 'bold',
  },
  otpCheck: {
    fontSize: 15,
    color: colors.charcoal,
    marginTop: 10
  },
  otpExpire: {
    marginVertical: '2%',
    fontSize: 14,
    color: colors.charcoal,
  },
  containerInput: {
    marginVertical: '5%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerCheckbox: {
    marginHorizontal: '8%',
    flexDirection: 'row',
    alignContent: 'center',
    marginTop: '18%',
  },
  checkBox: {
    left: '-18%',
  },
  countryCode: {
    color: colors.charcoal,
    fontSize: 20
  },
  checBoxText: {
    color: colors.charcoal,
    textAlignVertical: 'center',
    left: '25%',
    fontSize: 16,
  },
  containerDropdown: {
    marginHorizontal: '8%',
    marginVertical: '5%',
  },
  dropdownTitle: {
    color: colors.charcoal,
    fontSize: 16
  },
  dropdownInput: {
    left: '-1%',
    marginVertical: '5%',
    fontSize: 20
  },
  pickerRelation: {
    height: 60,
    width: '100%',
  },
  pickerCompany: {
    height: 60,
    width: '100%'
  },
  inputForm: {
    height: 50,
    width: '75%',
    fontSize: 20
  },
});
