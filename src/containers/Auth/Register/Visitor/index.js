import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Alert,
  StyleSheet,
  Keyboard,
  Animated,
  Easing,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Picker } from '@react-native-community/picker';
import CountryPicker from 'react-native-country-picker-modal';
import countriesList from '../../../../../countries.json';
import * as RNLocalize from 'react-native-localize';
var moment = require('moment')
import Snackbar from 'react-native-snackbar'

import { colors } from '../../../../helper/color';

import RoundedButton from '../../../../components/RoundedButton';

import { getPerusahaan } from '../../../../store/action/perusahaan';
import { getSubCategory } from '../../../../store/action/subCategory'
import { requestRegist } from '../../../../store/action/register';
import { goLoginVisitor } from '../../../../store/action/loginVisitor'
import { sendOTP } from '../../../../store/action/otpConfim';
import { formatPhoneNumber, getWidth, getHeight } from '../../../../helper/init';

class Register extends Component {
  constructor(props) {
    super(props);
    this.localNotify = null;
    this.senderiD = '333432402454';
    this.state = {
      visitorCheck: false,
      companyName: '',
      subCategory: '',
      companyPick: '',
      subPick: '',
      otpCode: '',
      nama: '',
      nik: '',
      phone: '',
      TokenFCM: '',
      result: '',
      timer: 59,
      Hcontainer: new Animated.Value(getHeight(5)),
      keyboardStatus: false,
      language: '',
      marginConatiner: '10%',
      buttonUp: 25,
      countryCode: 'ID',
      callingCode: '62',
      countriesList: countriesList
    };
  }
  async componentDidMount() {
    const { visitorCheck } = this.state;
    this.setState({
      visitorCheck: !visitorCheck,
    });
    // getFCM
    this.getTokenFCM();
    const { getPerusahaan, getSubCategory } = this.props;
    getPerusahaan();
    getSubCategory()
    let language = await AsyncStorage.getItem('language');
    // countryCode = RNLocalize.getLocales()[0].countryCode;
    this.setState({ language });
  }

  componentDidUpdate(prevProps) {
    const selectCompany = {
      id: null,
      company_name: this.state.language === 'id' ? 'Pilih Perusahaan' : 'Select Company'
    }
    if (prevProps.resPerusahaanOk != this.props.resPerusahaanOk) {
      this.setState({ companyName: this.props.resPerusahaanOk.data });
    }
    if (prevProps.subOk != this.props.subOk) {
      this.setState({ subCategory: this.props.subOk.data });
    }
    if (prevProps.logNonOk != this.props.logNonOk) {
      if (this.props.logNonOk.code === 200) {
        this.saveUserDetail(this.props.logNonOk);
      } else {
        Alert.alert(
          'Around Us',
          'Ada kesalahan saat login, silahkan ulangi kembali',
          [{ text: 'Oke', onPress: () => console.log('Ok pressed') }],
        );
      }
    }
  }

  UNSAFE_componentWillMount() {
    clearInterval(this.interval);
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );
  }

  keyboardDidShow = (e) => {
    const keyboardHeight = e.endCoordinates.height
    // console.log('Keyboard Shown', keyboardHeight);
    this.setState({ buttonUp: keyboardHeight - 115 });
  };

  keyboardDidHide = () => {
    // console.log('Keyboard Hidden');
    this.setState({ buttonUp: 25 });
    this.animationOff();
  };

  saveUserDetail = async data => {
    const { navigation } = this.props;
    const dateTime = moment().format('LLLL')
    // console.log('saveUserDetail ', data);
    await AsyncStorage.setItem('token', data.token);
    await AsyncStorage.setItem('lastLogin', dateTime);
    await AsyncStorage.setItem('service_id', data.profile.service_id)
    navigation.navigate('Permissions');
  };

  getTokenFCM = () => {
    AsyncStorage.getItem('tokenFCM', (error, result) => {
      if (result) {
        // alert(result)
        this.setState({
          TokenFCM: result,
        });
      }
    });
  };

  handleRegister = () => {
    const { goLoginVisitor } = this.props;
    const { nik, TokenFCM, nama, phone, companyPick, subPick, callingCode, language } = this.state;

    if (nama && nik && phone && companyPick != 'null' && subPick != 'null') {
      goLoginVisitor(nama, nik, phone, callingCode, companyPick, subPick, TokenFCM, language)
    } else {
      Alert.alert(
        'Around Us',
        'Salah satu parameter kosong, isi data terlebih dahulu!',
        [{ text: 'Oke', onPress: () => console.log('Ok Pressed') }],
      );
    }
  };

  animationOn = () => {
    Animated.timing(this.state.Hcontainer, {
      toValue: getHeight(1),
      duration: 180,
      useNativeDriver: true,
    }).start();
  };

  animationOff = () => {
    Animated.timing(this.state.Hcontainer, {
      toValue: getHeight(5),
      duration: 180,
      useNativeDriver: true,
    }).start();
  };

  render() {
    const { language, companyName, companyPick, marginConatiner, buttonUp, callingCode, countryCode, subCategory, subPick } = this.state;
    return (
      <View style={styles.container}>
        {this.props.network ? (
          Snackbar.dismiss()
        ) : (
          Snackbar.show({
            text: language == 'id' ? 'Tidak Ada Koneksi!' : 'No Internet Connection!',
            duration: Snackbar.LENGTH_INDEFINITE,
          })
        )}
        <View
          style={{
            marginHorizontal: '8%',
            marginVertical: marginConatiner
          }}>
          <Text style={styles.otpText}>
            {language == 'id' ? `Login sebagai Non Karyawan` : `Login as Visitor`}
          </Text>
        </View>
        <View style={styles.containerDropdown}>
          <Text style={styles.dropdownTitle}>
            {language == 'id' ? `Nama` : `Name`}
          </Text>
          <TextInput
            style={styles.dropdownInput}
            underlineColorAndroid={colors.charcoal}
            onChangeText={text => this.setState({ nama: text })}
            ref={input => {
              this.nameTextInput = input;
            }}
            returnKeyType={'next'}
            onSubmitEditing={() => {
              this.nikTextInput.focus();
            }}
            blurOnSubmit={false}
          />
          <Text style={styles.dropdownTitle}>
            {language == 'id' ? `NIK (16 digit)` : `ID CARD Number(16 digit)`}
          </Text>
          <TextInput
            maxLength={language == 'id' ? 16 : 99}
            style={styles.dropdownInput}
            underlineColorAndroid={colors.charcoal}
            keyboardType="number-pad"
            onChangeText={text => {
              this.setState({ nik: text });
            }}
            ref={input => {
              this.nikTextInput = input;
            }}
            returnKeyType={'next'}
            onSubmitEditing={() => {
              this.phoneTextInput.focus();
            }}
            blurOnSubmit={false}
          />
          <Text style={styles.dropdownTitle}>
            {language == 'id' ? `Nomor Telepon` : `Phone Number`}
          </Text>
          <View style={styles.containerInput}>
            <CountryPicker
              countryCode={countryCode}
              withCallingCode={true}
              onSelect={item => {
                this.setState({
                  countryCode: item.cca2,
                  callingCode: item.callingCode,
                });
              }}
            />
            <Text style={styles.countryCode}>{`+${callingCode}`}</Text>
            <TextInput
              ref={input => {
                this.phoneTextInput = input;
              }}
              keyboardType="number-pad"
              maxLength={15}
              selectionColor={colors.charcoal}
              underlineColorAndroid={colors.charcoal}
              style={styles.inputForm}
              onChangeText={text => this.setState({ phone: text })}
            />
          </View>
          <Text style={styles.dropdownTitle}>
            {language == 'id' ? `Perusahaan Relasi` : `Relasion Company`}
          </Text>
          <Picker
            selectedValue={companyPick}
            onValueChange={itemValue => {
              this.setState({ companyPick: itemValue });
            }}
            style={styles.pickerCompany}
          >
            <Picker.Item 
              label={language === 'id' ? 'Pilih Perusahaan' : 'Select Company'}
              value={'null'}
            />
            {companyName !== ''
              ? companyName.map((item, index) => {
                return (
                  <Picker.Item
                    key={index}
                    label={item.company_name}
                    value={item.id}
                  />
                );
              })
              : null}
          </Picker>
          <Text style={styles.dropdownTitle}>
            {language == 'id' ? `Sub Kategori` : `Sub Category`}
          </Text>
          <Picker
            selectedValue={subPick}
            onValueChange={itemValue => {
              this.setState({ subPick: itemValue });
            }}
            style={styles.pickerCompany}
          >
            <Picker.Item 
              label={language === 'id' ? 'Pilih Sub Kategori' : 'Select Sub Category'}
              value={'null'}
            />
            {subCategory !== ''
              ? subCategory.map((item, index) => {
                return (
                  <Picker.Item
                    key={index}
                    label={item.sub_category_name}
                    value={item.id}
                  />
                );
              })
              : null}
          </Picker>
        </View>
        <RoundedButton
          title={language == 'id' ? 'Lanjutkan' : 'Next'}
          onPress={() => this.handleRegister()}
          bottom={buttonUp}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  resPerusahaanOk: state.perusahaan.resPerusahaanOk,
  subOk: state.subCategory.subOk,
  logNonOk: state.loginVisitor.logNonOk,
  network: state.network.isConnected
});
const mapDispatchToProps = dispatch => ({
  getPerusahaan: () => dispatch(getPerusahaan()),
  getSubCategory: () => dispatch(getSubCategory()),
  goLoginVisitor: (name, nik, phone, callingCode, company, sub, token, language) => dispatch(goLoginVisitor(name, nik, phone, callingCode, company, sub, token, language))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  containerScroll: {
    marginBottom: '15%',
  },
  otpText: {
    fontSize: 20,
    color: colors.night_rider,
    // fontFamily: 'Cairo-Bold',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  otpCheck: {
    fontSize: 14,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Regular',
  },
  otpInput: {
    fontSize: 20,
    color: colors.charcoal,
    height: 50,
    textAlign: 'left',
    fontWeight: 'bold',
    // fontFamily: 'Cairo-Bold',
  },
  otpExpire: {
    marginVertical: '2%',
    fontSize: 14,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Regular',
  },
  containerCheckbox: {
    marginHorizontal: '8%',
    flexDirection: 'row',
    alignContent: 'center',
    marginTop: '18%',
  },
  checkBox: {
    left: '-18%',
  },
  checBoxText: {
    color: colors.charcoal,
    textAlignVertical: 'center',
    left: '25%',
    fontSize: 16,
  },
  containerDropdown: {
    marginHorizontal: '8%',
    marginVertical: '5%',
  },
  dropdownTitle: {
    color: colors.charcoal,
    fontSize: 16,
  },
  pickerCompany: {
    height: 60,
    width: '100%'
  },
  dropdownInput: {
    left: '-1%',
    marginVertical: '2%',
    fontSize: 16,
  },
  containerInput: {
    marginVertical: '5%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputForm: {
    height: 50,
    width: '75%',
    fontSize: 20
  },
  countryCode: {
    color: colors.charcoal,
    fontSize: 20
  },
});
