import React, {Component} from 'react';
import {Text, View, Image, TextInput, StyleSheet, Alert} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import CountryPicker from 'react-native-country-picker-modal';

import {LoginImage} from '../../../helper/image';
import {colors} from '../../../helper/color';

import RoundedButton from '../../../components/RoundedButton';
import {requestSendPhone} from '../../../store/action/sendPhone';
import {requestGantiPhone} from '../../../store/action/gantiPhone';
import {notificationManager} from '../../../helper/NotificationManager';
import * as RNLocalize from 'react-native-localize';
import countriesList from '../../../../countries.json';

class Login extends Component {
  constructor(props) {
    super(props);
    this.localNotify = null;
    this.senderiD = '333432402454';
    this.state = {
      isVisitor: false,
      phone: '',
      TokenFCM: '',
      countryCode: 'ID',
      callingCode: '62',
      language: '',
      countriesList: countriesList,
    };
  }
  async componentDidMount() {
    const {route} = this.props;
    const whoThis = route.params.whoThis;
    if (whoThis === 'visitor') {
      this.setState({isVisitor: true});
    } else {
      this.setState({isVisitor: false});
    }
    // get FCM
    await this.getTokenFCM();
    //PushNotif
    this.localNotify = notificationManager;
    this.localNotify.configure(
      this.onRegister,
      this.onNotification,
      this.onOpenNotification,
      this.senderiD,
    );
    let language = await AsyncStorage.getItem('language');
    this.setState({language});
  }

  componentDidUpdate(prevProps, prevState) {
    const {navigation} = this.props;
    const {callingCode, phone} = this.state;

    if (prevProps.respSendOk != this.props.respSendOk) {
      console.log('componentDidUpdate ', this.props.respSendOk.code);
      navigation.navigate('RegisterVisitor', {
        otp: this.props.respSendOk.otp,
        noPhone: `+${callingCode}${phone}`,
        register: this.props.respSendOk.code,
      });
    }

    if (prevProps.respGantiPhone != this.props.respGantiPhone) {
      console.log('[componentDidUpdate][gantiPhone]', this.props.respGantiPhone);
      if (this.props.respGantiPhone.code == 200) {
        navigation.navigate('Permissions');
      } else {
        Alert.alert('Around Us', this.props.respGantiPhone.message, [
          {text: 'Oke', onPress: () => console.log('Ok Pressed')},
        ]);
      }
    }
  }

  onRegister(token) {
    // console.log('[Notification] Registered', token.token);
  }

  onNotification(notify) {
    // console.log('[Notification] onNotification', notify);
  }

  async onOpenNotification(notify) {
    // console.log('[Notification] onOpenNotification', notify);
    await AsyncStorage.setItem('notify', notify.message);
  }

  getTokenFCM = async () => {
    await AsyncStorage.getItem('tokenFCM', (error, result) => {
      if (result) {
        // alert(result)
        this.setState({
          TokenFCM: result,
        });
      }
    });
  };

  handleDataPhone = async () => {
    const {requestSendPhone, requestGantiPhone} = this.props;
    const {phone, TokenFCM, callingCode, isVisitor, language} = this.state;

    if (isVisitor) {
      const userPhone = '+' + callingCode + phone;
      console.log('handleDataPhone ', userPhone, TokenFCM);
      await AsyncStorage.setItem('userPhone', userPhone);
      if(userPhone.length > 3) {
        // console.log('[userPhone]', userPhone)
        requestSendPhone(userPhone, TokenFCM);
      } else {
        Alert.alert(
          'Around Us',
          language == 'id' ? 'Isikan Nomor Telepon' : 'Fill Phone Number',
          [{text: 'Oke', onPress: () => console.log('Ok pressed')}],
        );
      }
    } else {
      requestGantiPhone(phone, callingCode);
    }
  };

  render() {
    const {countryCode, callingCode, language} = this.state;
    return (
      <View style={styles.container}>
        <Image style={styles.ilustrasi} source={LoginImage.LoginIlust} />
        <View style={styles.containerForm}>
          <Text style={styles.titleForm}>
            {this.state.isVisitor
              ? language == 'id'
                ? `Daftar/Login`
                : `Register/Login`
              : language == 'id'
              ? `Ganti Nomor Telepon`
              : `Change Phone Number`}
          </Text>
          <Text style={styles.headerForm}>
            {language == 'id'
              ? `Masukkan No Hp Anda`
              : `Enter your Phone number`}
          </Text>
        </View>
        <View style={styles.containerInput}>
          <CountryPicker
            countryCode={countryCode}
            withCallingCode={true}
            onSelect={item => {
              this.setState({
                countryCode: item.cca2,
                callingCode: item.callingCode,
              });
            }}
          />
          <Text style={styles.countryCode}>{`+${callingCode}`}</Text>
          <TextInput
            keyboardType="number-pad"
            maxLength={15}
            selectionColor={colors.charcoal}
            underlineColorAndroid={colors.charcoal}
            style={styles.inputForm}
            onChangeText={text => this.setState({phone: text})}
          />
        </View>
        <RoundedButton
          onPress={() => this.handleDataPhone()}
          title={
            this.state.isVisitor
              ? language == 'id'
                ? 'Bergabung'
                : 'Join Us'
              : language == 'id'
              ? 'Ganti Nomor Telepon'
              : 'Change Phone Number'
          }
          bottom={'5%'}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  respSendOk: state.sendPhone.respSendOk,
  respGantiPhone: state.gantiPhone.respGantiOk,
});
const mapDispatchToProps = dispatch => ({
  requestSendPhone: (phone, token) => dispatch(requestSendPhone(phone, token)),
  requestGantiPhone: (phone, callingCode) => dispatch(requestGantiPhone(phone, callingCode)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  ilustrasi: {
    top: -60,
    width: '75%',
    height: '80%',
    resizeMode: 'contain',
  },
  containerForm: {
    top: -140,
    left: '-18%',
  },
  titleForm: {
    // fontFamily: 'Cairo-Bold',
    fontSize: 18,
    fontWeight: 'bold',
  },
  headerForm: {
    fontSize: 16,
    color: colors.charcoal,
    marginTop: 5,
    // fontFamily: 'Cairo-Regular',
  },
  containerInput: {
    top: -130,
    flexDirection: 'row',
    alignItems: 'center',
  },
  countryCode: {
    color: colors.charcoal,
    fontSize: 20,
    // fontFamily: 'Cairo-Regular',
  },
  inputForm: {
    top: 0,
    height: 50,
    width: '65%',
    fontSize: 20,
    // fontFamily: 'Cairo-Regular',
  },
});
