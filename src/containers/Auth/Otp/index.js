import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {colors} from '../../../helper/color';

import RoundedButton from '../../../components/RoundedButton';
import {sendOTP} from '../../../store/action/otpConfim';
import AsyncStorage from '@react-native-community/async-storage';
import {formatPhoneNumber} from '../../../helper/init';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      relasiCheck: false,
      relasionTo: 'Ayah',
      companyName: 'A',
      timer: 59,
      otp: this.props.route.params.otp,
    };
  }
  componentDidMount() {
    const {relasiCheck} = this.state;
    this.setState({relasiCheck: !relasiCheck});
    this.interval = setInterval(
      () => this.setState(prevState => ({timer: prevState.timer - 1})),
      2000,
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.respOtpOk != this.props.respOtpOk) {
      if (this.props.respOtpOk.code == 200) {
        AsyncStorage.setItem('token', this.props.respOtpOk.token);
        this.props.navigation.navigate('Permissions');
      } else {
        Alert.alert('Around Us', this.props.respOtpOk.message, [
          {text: 'Oke', onPress: () => console.log('Ok Pressed')},
        ]);
      }
    }

    if (this.state.timer === 1) {
      clearInterval(this.interval);
    }
  }

  UNSAFE_componentWillMount() {
    clearInterval(this.interval);
  }

  confirmOTP = () => {
    console.log('confirmOTP ', this.state.otp);
    if (this.state.otp == '') {
      Alert.alert('Around Us', 'OTP tidak boleh kosong!', [
        {text: 'Oke', onPress: () => console.log('Ok Pressed')},
      ]);
    } else {
      this.props.sendOTP(this.state.otp);
    }
  };

  showForm = () => {
    const {relasiCheck} = this.state;
    this.setState({relasiCheck: !relasiCheck});
  };

  render() {
    const {navigation} = this.props;
    const {timer} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.containerOTP}>
          {/* <Text style={styles.otpText}>Masukan Kode OTP</Text>
          <Text style={styles.otpCheck}>Cek SMS Kode OTP di Pesan Anda</Text> */}
          <Text
            style={
              styles.otpCheck
            }>{`Kami telah mengirimkan Kode OTP ke\n${formatPhoneNumber(
            this.props.route.params.noPhone,
          )},\nCek SMS Kode OTP di Pesan Anda`}</Text>
          <TextInput
            ref={'textInput'}
            style={styles.otpInput}
            underlineColorAndroid={'transparent'}
            placeholder={'____ ____ ____ ____ ____ ____'}
            selectionColor={colors.charcoal}
            autoFocus
            maxLength={6}
            keyboardType="name-phone-pad"
            value={this.state.otp}
            onChangeText={text => this.setState({otp: text})}
          />
          <Text style={styles.otpExpire}>
            {`Kode anda kadaluarsa dalam 00:${timer}`}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.otpExpire}>
              {`Bila Anda tidak dapat menerima OTP bisa melakukan`}
            </Text>
          </View>
          <TouchableOpacity
            style={{justifyContent: 'center'}}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.otpReset}>{`Registrasi Ulang No HP Anda`}</Text>
          </TouchableOpacity>
        </View>
        <RoundedButton
          title="Lanjutkan"
          onPress={() => this.confirmOTP()}
          bottom={'5%'}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  respOtpOk: state.otpConfirm.respOtpOk,
});
const mapDispatchToProps = dispatch => ({
  sendOTP: otp => dispatch(sendOTP(otp)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  containerScroll: {
    marginBottom: '15%',
  },
  containerOTP: {
    marginHorizontal: '8%',
    marginVertical: '10%',
  },
  otpText: {
    fontSize: 20,
    color: colors.night_rider,
    // fontFamily: 'Cairo-Bold',
    fontWeight: 'bold',
  },
  otpCheck: {
    fontSize: 14,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Regular',
  },
  otpInput: {
    fontSize: 20,
    color: colors.charcoal,
    height: 50,
    textAlign: 'left',
    fontWeight: 'bold',
  },
  otpExpire: {
    marginVertical: '2%',
    fontSize: 13,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Regular',
  },
  otpReset: {
    marginVertical: '2%',
    fontSize: 13,
    color: colors.jacksons_purple,
    textDecorationLine: 'underline',
    // fontFamily: 'Cairo-Bold',
    fontWeight: 'bold',
  },
  containerCheckbox: {
    marginHorizontal: '8%',
    flexDirection: 'row',
    alignContent: 'center',
    marginTop: '18%',
  },
  checkBox: {
    left: '-18%',
  },
  checBoxText: {
    color: colors.charcoal,
    textAlignVertical: 'center',
    left: '25%',
    fontSize: 16,
  },
  containerDropdown: {
    marginHorizontal: '8%',
    marginVertical: '5%',
  },
  dropdownTitle: {
    color: colors.charcoal,
    fontSize: 16,
  },
  dropdownInput: {
    left: '-1%',
    marginVertical: '5%',
    fontSize: 16,
  },
  pickerRelation: {
    height: 60,
    width: '100%',
  },
  pickerCompany: {
    height: 60,
    width: '100%',
  },
});
