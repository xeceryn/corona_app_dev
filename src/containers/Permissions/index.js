import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  PermissionsAndroid,
  BackHandler,
} from 'react-native';
import BleManager from 'react-native-ble-manager';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-community/async-storage'
import {colors} from '../../helper/color';
import {PermissionImage} from '../../helper/image';

import RoundedButton from '../../components/RoundedButton';

class Permissions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      granted: false,
      language: '',
    };
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    const language = await AsyncStorage.getItem('language');
    this.setState({language: language});
  }

  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  onButtonPress = () => {
    if (this.props.route.key)
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.handleBackButton,
      );
    // then navigate
    navigate('NewScreen');
  };

  handleBackButton = () => {
    alert(JSON.stringify(this.props));
    if (this.props.route.name == 'Permissions') {
      BackHandler.exitApp();
      // alert('kill app')
      return true;
    }
    // alert('back app')
    return false;
  };

  getPermissions = () => {
    this.bluetoothPerm();
  };
  bluetoothPerm = () => {
    BleManager.start({showAlert: false}).then(() => {
      BleManager.enableBluetooth()
        .then(() => {
          this.setState({granted: true});
          this.getGeoLocation();
        })
        .catch(() => {
          this.setState({granted: false});
        });
    });
  };
  getGeoLocation = () => {
    var that = this;
    //Checking for the permission just after component loaded
    if (Platform.OS === 'ios') {
      this.callLocation(that);
    } else {
      async function requestLocationPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            that.callLocation(that);
          } else {
            alert('Permission Denied');
          }
        } catch (err) {
          // alert('err', err);
          console.warn(err);
        }
      }
      requestLocationPermission();
    }
  };
  callLocation(that) {
    //alert("callLocation Called");
    Geolocation.getCurrentPosition(
      //Will give you the current location
      position => {
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json
        that.setState({currentLongitude: currentLongitude});
        //Setting state Longitude to re re-render the Longitude Text
        that.setState({currentLatitude: currentLatitude});
        //Setting state Latitude to re re-render the Longitude Text
      },
      error => console.log(error.message),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
    that.watchID = Geolocation.watchPosition(position => {
      //Will give you the location on location change
      console.log(position);
      const currentLongitude = JSON.stringify(position.coords.longitude);
      //getting the Longitude from the location json
      const currentLatitude = JSON.stringify(position.coords.latitude);
      //getting the Latitude from the location json
      that.setState({currentLongitude: currentLongitude});
      //Setting state Longitude to re re-render the Longitude Text
      that.setState({currentLatitude: currentLatitude});
      //Setting state Latitude to re re-render the Longitude Text
    });
  }
  render() {
    const {navigation} = this.props;
    const {granted, language} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.containerTitle}>
          <Image source={PermissionImage.BT_Perm} style={styles.imagePerm} />
          <Text style={styles.permissionsTitle}>
            {granted
              ? language == 'id'
                ? 'Aplikasi Berhasil Mengakses'
                : 'Applications Successfully Accessing'
              : language == 'id'
              ? 'Izinkan Aplikasi Mengakses'
              : 'Allow Applications to Access'}
          </Text>
        </View>
        <View style={styles.containerContent}>
          <View style={styles.containerContentItem}>
            <Text style={styles.permissionName}>1. Bluetooth</Text>
            {granted ? <Image source={PermissionImage.PermOk} /> : null}
          </View>
          <View style={styles.containerContentItem}>
            <Text style={styles.permissionName}>2. Location Permissions</Text>
            {granted ? <Image source={PermissionImage.PermOk} /> : null}
          </View>
          <View style={styles.containerContentItem}>
            <Text style={styles.permissionName}>3. Battery Optimizer</Text>
            {granted ? <Image source={PermissionImage.PermOk} /> : null}
          </View>
          <View style={styles.containerContentItem}>
            <Text style={styles.permissionName}>4. Push Notification</Text>
            {granted ? <Image source={PermissionImage.PermOk} /> : null}
          </View>
        </View>
        <View style={styles.containerWarning}>
          <Text style={styles.permissionWarning}>
            {language == 'id'
              ? `Around Us membutuhkan Location Permissions agar bluetooth dapat bekerja.`
              : 'Around Us Android requires Location Permissions for Bluetooth to work.'}
          </Text>
        </View>
        {granted ? (
          <View style={styles.containerAttention}>
            <Text style={styles.permissionAttention}>
              {language == 'id'
                ? `Pastikan Aplikasi tetap terbuka di background sampai kasus ditutup`
                : `Make sure the Application remains open in the background until the case is closed`}
            </Text>
          </View>
        ) : null}
        <RoundedButton
          title={
            granted
              ? language == 'id'
                ? 'Lanjutkan'
                : 'Next'
              : language == 'id'
              ? 'Izinkan'
              : 'Allow All Permissions'
          }
          onPress={
            granted
              ? () => navigation.navigate('Root', {back: true})
              : () => this.getPermissions()
          }
          bottom={'5%'}
        />
      </View>
    );
  }
}
export default Permissions;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  containerTitle: {
    // top: '30%',
    marginHorizontal: '10%',
    marginVertical: '10%',
  },
  imagePerm: {
    // top: -180,
    resizeMode: 'contain',
    width: '80%',
    height: '50%',
    alignSelf: 'center',
    marginBottom: '15%',
  },
  permissionsTitle: {
    fontSize: 20,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Bold',
    fontWeight: 'bold',
  },
  containerContent: {
    top: '-18%',
    marginHorizontal: '10%',
  },
  containerContentItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  permissionName: {
    fontSize: 20,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Regular',
  },
  containerWarning: {
    top: '-12%',
    marginHorizontal: '10%',
  },
  permissionWarning: {
    fontSize: 14,
    color: colors.charcoal,
    // fontFamily: 'Cairo-Regular',
  },
  containerAttention: {
    top: '-9%',
    marginHorizontal: '8%',
  },
  permissionAttention: {
    fontSize: 15,
    color: colors.charcoal,
    width: '105%',
    // fontFamily: 'Cairo-Regular',
  },
});
