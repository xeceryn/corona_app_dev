import React from 'react';
import {Text, View, Image} from 'react-native';
import {getHeight, getWidth} from '../../helper/init';
import {colors} from '../../helper/color';
import {HomeImage, Animasi} from '../../helper/image';

import {Switch} from 'react-native-switch';
import DeviceInfo from 'react-native-device-info';

const StatusCard = ({
  params,
  TrackingMode,
  data,
  dataTrackingData,
  AreaUmum,
  LastUpdate,
  appOn,
  language,
  tracingStatus,
  SwitchStatus,
  loading,
}) => (
  <View
    style={{
      backgroundColor: '#FCECEC',
      padding: getWidth(4.4),
    }}>
    <View style={{position: 'absolute', right: getWidth(5), top: getHeight(4)}}>
      <Switch
        value={tracingStatus}
        onValueChange={SwitchStatus}
        disabled={false}
        circleSize={25}
        circleBorderWidth={1}
        backgroundActive={'white'}
        backgroundInactive={'white'}
        circleActiveColor={colors.red}
        circleInActiveColor={'black'}
        renderActiveText={false}
        renderInActiveText={false}
        renderInsideCircle={() => (
          <Text style={{color: '#fff', fontWeight: 'bold'}}>
            {tracingStatus ? 'On' : 'Off'}
          </Text>
        )}
      />
    </View>

    {appOn ? (
      <View>
        {TrackingMode ? (
          <View
            style={{
              alignSelf: 'center',
              borderRadius: getWidth(3),
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: AreaUmum ? '#E2374F' : '#E2A228',
              height: getHeight(13),
              width: getWidth(89),
              marginTop: getHeight(8.3),
              padding: getWidth(5),
            }}>
            <Text
              style={{
                fontSize: getWidth(6.5),
                fontWeight: 'bold',
                color: '#FFF',
              }}>
              {AreaUmum
                ? language == 'id'
                  ? `Anda sedang dalam kerumunan !`
                  : `You're in a crowd!`
                : language == 'id'
                ? `Anda berdekatan dengan Orang lain`
                : `You are close to other people`}
            </Text>
          </View>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: getHeight(2.2),
            }}>
            {loading ? (
              <Image
                style={{height: getHeight(15), width: getHeight(15)}}
                source={Animasi.tracing}
                resizeMode="contain"
              />
            ) : (
              <Image
                style={{height: getHeight(15), width: getHeight(15)}}
                source={HomeImage.Home_STAY}
                resizeMode="contain"
              />
            )}
          </View>
        )}
      </View>
    ) : (
      <View
        style={{
          alignSelf: 'center',
          borderRadius: getWidth(3),
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#FFF',
          height: getHeight(13),
          width: getWidth(89),
          marginTop: getHeight(4.3),
          padding: getWidth(5),
        }}>
        <Text
          style={{
            fontSize: getWidth(6.5),
            fontWeight: 'bold',
            color: '#000',
          }}>
          {language == 'id'
            ? `Status aplikasi nonaktif`
            : `Application status is off`}
        </Text>
      </View>
    )}

    <View
      style={{
        alignSelf: 'center',
        marginTop: getHeight(2.5),
        width: getWidth(84),
      }}>
      <Text
        style={{
          fontSize: getWidth(4.5),
          fontWeight: 'bold',
        }}>
        {language == 'id'
          ? `Total Berdekatan Harian`
          : `Total Close Together Daily`}
      </Text>
      <Text
        style={{
          fontSize: getWidth(4.5),
          color: '#E2374F',
          fontWeight: 'bold',
        }}>
        {language == 'id'
          ? `${dataTrackingData ? dataTrackingData.daily : '0'} Kali`
          : `${dataTrackingData ? dataTrackingData.daily : '0'} Times`}
      </Text>
    </View>
    <View
      style={{
        alignSelf: 'center',
        marginTop: getHeight(2.5),
        marginBottom: getHeight(4),
        width: getWidth(84),
      }}>
      <Text
        style={{
          fontSize: getWidth(4.5),
          fontWeight: 'bold',
        }}>
        {language == 'id'
          ? `Total Berdekatan Mingguan`
          : `Total Close Together Weekly`}
      </Text>
      <Text
        style={{
          fontSize: getWidth(4.5),
          color: '#E2374F',
          fontWeight: 'bold',
        }}>
        {language == 'id'
          ? `${dataTrackingData ? dataTrackingData.weekly : '0'} Kali`
          : `${dataTrackingData ? dataTrackingData.weekly : '0'} Times`}
      </Text>
    </View>
    <View
      style={{
        position: 'absolute',
        bottom: getHeight(1),
        right: getWidth(8),
        flexDirection: 'row',
      }}>
      <Text style={{color: '#898b8c', fontWeight: 'bold', right: getWidth(1)}}>
        {language === 'id'
          ? `Versi ${DeviceInfo.getVersion()},`
          : `Version ${DeviceInfo.getVersion()},`}
      </Text>
      <Text
        style={{
          color: '#848484',
          fontWeight: 'bold',
        }}>{`Last Active ${LastUpdate}`}</Text>
    </View>
  </View>
);

export default StatusCard;
