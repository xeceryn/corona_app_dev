import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Text,
  View,
  Image,
  PermissionsAndroid,
  Platform,
  BackHandler,
  InteractionManager,
  Modal,
  StyleSheet,
  TouchableHighlight,
  Linking,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getHeight, getWidth} from '../../helper/init';
import {HomeImage} from '../../helper/image';
import {TouchableOpacity} from 'react-native-gesture-handler';
import StatusCard from './StatusCard';
import {getTracking} from '../../store/action/home';
import { recordTracing } from '../../store/action/recordTracing'
import {saveCardAction} from '../../store/action/saveCard';
import {getTrackingData} from '../../store/action/TrackingData';
import {getPerusahaan} from '../../store/action/perusahaan';
import {getHubungan} from '../../store/action/hubKeluarga';
import {getUpdateApp} from '../../store/action/updateApp';
import {getRiwayatInterkasi} from '../../store/action/RiwayatInteraksi';
// import Geolocation from '@react-native-community/geolocation';
// import { ArrowIcon } from '../../helper/image';
// import BleManager from 'react-native-ble-manager';
import BluetoothSerial from 'react-native-bluetooth-serial-next';
import DeviceInfo from 'react-native-device-info';
import BackgroundJob from 'react-native-background-actions';
import moment from 'moment';
import {notificationManager} from '../../helper/NotificationManager/';
// import Geolocation from 'react-native-geolocation-service';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
// import { BASE_URL, TRACKING } from '../../constants';
import Share from 'react-native-share';
import Snackbar from 'react-native-snackbar'
import {requestTraceFitur} from '../../store/action/traceFitur';
import {medianAltitude} from '../../helper/function';
import {setVersion} from '../../store/action/updateVersi';
import SpecialBle from 'rn-contact-tracing'

const CurrentTime = moment().format(' HH:mm MMMM DD YYYY');

class Home extends Component {
  playing = BackgroundJob.isRunning();
  message = 'Status anda aman';
  notif = 0;

  constructor(props) {
    super(props);
    this.localNotify = null;
    this.state = {
      appOn: true,
      TrackingMode: false,
      dataTracking: null,
      dataTrackingData: null,
      AreaUmum: false,
      statusBT: true,
      statusGPS: true,
      devices: [],
      lastUpdate: null,
      currentLongitude: 'unknown', //Initial Longitude
      currentLatitude: 'unknown', //Initial Latitude,
      vAndroid: DeviceInfo.getBuildNumber(),
      vIOS: DeviceInfo.getBuildNumber(),
      LinkAndroid: '',
      LinkIOS: '',
      updateIOS: false,
      updateANDROID: false,
      urlRiwayat: '',
      language: '',
      tracingStatus: true,
    };
  }

  onShare = () => {
    const {language} = this.state;
    try {
      const shareOptions = {
        title: language == 'id' ? 'Bagikan via ' : 'Share via',
        message:
          language == 'id'
            ? `Mari lindungi diri anda dari virus Covid-19, download Aplikasi Around US: \n ${
                Platform.OS == 'ios'
                  ? this.state.LinkIOS
                  : this.state.LinkAndroid
              }`
            : `Let's protect yourself from the Covid-19 virus, download the Around US Application: \n ${
                Platform.OS == 'ios'
                  ? this.state.LinkIOS
                  : this.state.LinkAndroid
              }`,
      };
      Share.open(shareOptions);
    } catch (error) {
      // alert(error.message);
    }
  };

  /**
   * Toggles the background task
   */
  backgroundTask = async () => {
    const {language} = this.state;
    try {
      console.log('Trying to start background service');
      const options = {
        taskName: 'Background Services',
        taskTitle: 'Around Us',
        taskDesc:
          language === 'id' ? 'Berjalan di Background' : 'Run in Background',
        taskIcon: {
          name: 'ic_launcher',
          type: 'mipmap',
        },
        color: '#000',
        parameters: {
          delay: 20000,
        },
      };
      await BackgroundJob.start(this.taskRandom, options);
      console.log('Successful start!');
    } catch (e) {
      console.log('Error', e);
    }
  };

  stopTask = async () => {
    console.log('Stop background service');
    await BackgroundJob.stop();
  };

  taskRandom = async taskData => {
    if (Platform.OS === 'ios') {
      console.warn(
        'This task will not keep your app alive in the background by itself, use other library like react-native-track-player that use audio,',
        'geolocalization, etc. to keep your app alive in the background while you excute the JS from this library.',
      );
    }
    await new Promise(async resolve => {
      const {language} = this.state;
      let notif = 0;
      if (BackgroundJob.isRunning()) {
        const isBluetoothEnable = await BluetoothSerial.isEnabled();
        const isLocationEnable = await DeviceInfo.isLocationEnabled();
        // console.log('[BT | LOCATION]', isBluetoothEnable, isLocationEnable)
        if (!isBluetoothEnable && !isLocationEnable) {
          // console.log('[0]', isBluetoothEnable, isLocationEnable)
          this.sendNotification(
            language === 'id'
              ? 'Bluetooth dan GPS mati'
              : 'Bluetooth and GPS are off',
          );
          BackgroundGeolocation.stop();
        } else if (!isBluetoothEnable && isLocationEnable) {
          // console.log('[1]', isBluetoothEnable, isLocationEnable)
          this.sendNotification(
            language === 'id' ? 'Bluetooth mati' : 'Bluetooth is off',
          );
          BackgroundGeolocation.stop();
        } else if (isBluetoothEnable && !isLocationEnable) {
          // console.log('[2]', isBluetoothEnable, isLocationEnable)
          this.sendNotification(
            language === 'id' ? 'Lokasi GPS mati' : 'GPS location is off',
          );
          BackgroundGeolocation.stop();
        } else if (isBluetoothEnable && isLocationEnable) {
          // console.log('[3]', isBluetoothEnable, isLocationEnable)
          BackgroundGeolocation.start();
          BackgroundGeolocation.on('location', async location => {
            // Altitude
            // const altitude = location.altitude.toString()
            // ToastAndroid.show(Math.floor(altitude), ToastAndroid.SHORT)
            const altitude = Math.floor(location.altitude);
            // const mean = JSON.stringify(medianAltitude(altitude))
            // value altitude asli
            // console.log('[altitude]', altitude)
            // value altitude filter median
            // ToastAndroid.show(mean, ToastAndroid.SHORT)
            // console.log('[median]', medianAltitude(altitude))
            const payload = {
              latitude: location.latitude,
              longitude: location.longitude,
              is_bluetooth: isBluetoothEnable ? 1 : 0,
              // need fix value altitude
              altitude: medianAltitude(altitude),
            };
            this.props.recordTracing(payload);
            taskKey = this.props.getTracking(payload);
            // console.log('[INFO][BackgroundAction]', payload);
            BackgroundGeolocation.startTask(taskKey => {
              BackgroundGeolocation.endTask(taskKey);
            });
          });
        } else {
          // console.log('[4]', isBluetoothEnable, isLocationEnable)
          // console.log('[Tidak Memenuhi]')
        }
      }
    });
  };

  async componentDidMount() {
    let language = await AsyncStorage.getItem('language');
    this.setState({language});
    let tracingStatus = await AsyncStorage.getItem('BackgroundService');
    // console.log('[Didmount]', tracingStatus)
    this.setState({lastUpdate: CurrentTime});
    if (tracingStatus === null) {
      this.props.setVersion();
      this.setState({tracingStatus: true});
      AsyncStorage.setItem('BackgroundService', 'true');
    } else if (tracingStatus === 'false') {
      this.setState({tracingStatus: false});
      AsyncStorage.setItem('BackgroundService', 'false');
    } else if (tracingStatus === 'true') {
      this.props.setVersion();
      this.setState({tracingStatus: true});
      AsyncStorage.setItem('BackgroundService', 'true');
    }
    InteractionManager.runAfterInteractions(async () => {
      // await this.localNotify.cancelAllNotification();
      // const lastUpdate = await AsyncStorage.getItem('LastUpdate');
      const openArea = await AsyncStorage.getItem('AreaUmum');
      const token = await AsyncStorage.getItem('token');
      const notif = await AsyncStorage.getItem('notify');

      // alert('notif: '+notif)
      // console.log('[lastUpdate]', lastUpdate)
      // if (lastUpdate) {
      //   this.setState({ lastUpdate: CurrentTime });
      // } else {
      //   this.setState({ lastUpdate: CurrentTime });
      // }
      if (openArea == 'true') {
        this.setState({AreaUmum: true, TrackingMode: true});
      } else if (openArea == 'false') {
        this.setState({AreaUmum: false, TrackingMode: true});
      } else {
        this.setState({AreaUmum: false, TrackingMode: false});
      }
      // alert(openArea);
      await this.stopTask();
      BackgroundGeolocation.configure({
        desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
        stationaryRadius: 50,
        distanceFilter: 50,
        debug: false,
        locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
        notificationTitle: language == 'id' ? 'Around Us' : 'Around Us',
        notificationText:
          language == 'id'
            ? 'Restart Aplikasi jika tidak menerima notifikasi'
            : 'Restart Application if notification not shown',
        startForeground: true,
        startOnBoot: false,
        stopOnTerminate: false,
        saveBatteryOnBackground: false,
        interval: 30000,
        fastestInterval: 12000,
        activitiesInterval: 10000,
        stopOnStillActivity: false,
        // url: `${BASE_URL}${TRACKING}`,
        url: '',
        // httpHeaders: {
        //   'Content-Type': `application/json`,
        //   Authorization: `Bearer ${token}`,
        // },
        httpHeaders: {},
        // postTemplate: {
        //   latitude: '@latitude',
        //   longitude: '@longitude',
        //   is_bluetooth: 1,
        // },
        postTemplate: {},
      });
      BackgroundGeolocation.on('location', async location => {
        // handle your locations here
        // to perform long running operation on iOS
        const isBluetoothEnable = await BluetoothSerial.isEnabled();
        const altitude = Math.floor(location.altitude);
        // value altitude asli
        // console.log('[altitude]', altitude)
        // value altitude filter median
        // console.log('[median]', medianAltitude(altitude))
        const payload = {
          latitude: location.latitude,
          longitude: location.longitude,
          is_bluetooth: isBluetoothEnable ? 1 : 0,
          // need fix value altitude
          altitude: medianAltitude(altitude),
        };
        this._startScanBle();
        // taskKey = this.props.getTracking(payload);
        // this.props.recordTracing(payload);
        // this.props.getTrackingData(payload);
        // you need to create background task
        // console.log('[INFO][BackgroundGeolocation]', payload);
        BackgroundGeolocation.startTask(taskKey => {
          // execute long running task
          // eg. ajax post location
          // IMPORTANT: task has to be ended by endTask
          BackgroundGeolocation.endTask(taskKey);
        });
      });
      BackgroundGeolocation.on('stationary', stationaryLocation => {
        // handle stationary locations here
        // Actions.sendLocation(stationaryLocation);
      });

      BackgroundGeolocation.on('error', error => {
        // console.log('[ERROR] BackgroundGeolocation error:', error);
      });

      BackgroundGeolocation.on('start', () => {
        // console.log('[INFO] BackgroundGeolocation service has been started');
      });

      BackgroundGeolocation.on('stop', () => {
        // console.log('[INFO] BackgroundGeolocation service has been stopped');
      });

      BackgroundGeolocation.on('authorization', status => {
        console.log(
          // '[INFO] BackgroundGeolocation authorization status: ' + status,
        );
        if (status == 1) {
          this.checkLocation();
          this.sendNotification('Gps location anda mati');
        }
        if (status !== BackgroundGeolocation.AUTHORIZED) {
          // we need to set delay or otherwise alert may not be shown
          setTimeout(
            () =>
              Alert.alert(
                'App requires location tracking permission',
                'Would you like to open app settings?',
                [
                  {
                    text: 'Yes',
                    onPress: () => BackgroundGeolocation.showAppSettings(),
                  },
                  {
                    text: 'No',
                    onPress: () => console.log('No Pressed'),
                    style: 'cancel',
                  },
                ],
              ),
            1000,
          );
        }
      });

      BackgroundGeolocation.on('background', () => {
        console.log('[INFO] App is in background');
        BackgroundGeolocation.start();
      });

      BackgroundGeolocation.on('foreground', () => {
        console.log('[INFO] App is in foreground');
      });

      BackgroundGeolocation.on('abort_requested', () => {
        console.log('[INFO] Server responded with 285 Updates Not Required');

        // Here we can decide whether we want stop the updates or not.
        // If you've configured the server to return 285, then it means the server does not require further update.
        // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
        // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
      });

      BackgroundGeolocation.on('http_authorization', () => {
        console.log('[INFO] App needs to authorize the http requests');
      });

      BackgroundGeolocation.checkStatus(status => {
        // console.log(
        //   '[INFO] BackgroundGeolocation service is running',
        //   status.isRunning,
        // );
        // console.log(
        //   '[INFO] BackgroundGeolocation services enabled',
        //   status.locationServicesEnabled,
        // );
        // console.log(
        //   '[INFO] BackgroundGeolocation auth status: ' + status.authorization,
        // );

        // you don't need to check status before start (this is just the example)
        if (!status.isRunning) {
          BackgroundGeolocation.start(); //triggers start on start event
        }
      });
      await this.props.getUpdateApp();
      await this.props.getTrackingData();
      await this.props.getPerusahaan();
      await this.props.getHubungan();
      // await this.props.getRiwayatInterkasi();
      await this.checkLocation();
      this.localNotify = notificationManager;
      this.localNotify.configure(
        this.onRegister,
        this.onNotification,
        this.onOpenNotification,
      );
    });
  }

  onRegister(token) {}

  onNotification(notify) {
    console.log('onNotification', JSON.stringify(notify));
  }

  async onOpenNotification(notify) {
    console.log('onOpenNotification');
    // alert(JSON.stringify(notify.message));
    await AsyncStorage.setItem('notify', notify.message);
  }

  sendNotification = async msg => {
    await this.localNotify.cancelAllNotification();
    await this.localNotify.showNotification(
      1,
      'Around Us',
      msg,
      {}, //data
      {}, //options
    );
  };

  async componentDidUpdate(prevProps, prevState) {
    const {language} = this.state;
    if (prevProps.respTrackingData !== this.props.respTrackingData) {
      // console.log('REST respTracking ==> ', this.props.respTracking);
      const prevTracing = prevProps.respTrackingData;
      // console.log('[pre ?]', prevTracing)
      if (this.props.respTrackingData.code === 200) {
        const timeUpdated = this.props.respTrackingData.bersinggungan.last_update;
        const time = timeUpdated.slice(10, 16);
        const timeNow = time+' '+moment().format('MMMM DD YYYY')
        // console.log('[timeUpdate]', timeNow);
        await this.setState({
          dataTracking: this.props.respTrackingData.bersinggungan,
          dataTrackingData: this.props.respTrackingData.bersinggungan,
          appOn: this.props.respTrackingData.tracking_mode === 1 ? true : false,
          lastUpdate: timeNow,
        });
        if (prevTracing !== null) {
          const daily = prevTracing.bersinggungan.daily;
          this.restTrace(
            daily,
            this.props.respTrackingData.bersinggungan.daily,
            this.props.respTrackingData.jumlah,
          );
          // AsyncStorage.setItem('LastUpdate', CurrentTime);
        }
      } else {
        const isEnabled = await BluetoothSerial.isEnabled();
        DeviceInfo.isLocationEnabled().then(enabled => {
          if (isEnabled === false && enabled === false) {
            // console.log('BT Gps Mati');
            // this.notificationManager.cancelAllNotification();
            if (this.notif === 0) {
              this.sendNotification(
                language === 'id'
                  ? 'Bluetooth dan GPS mati'
                  : 'Bluetooth and GPS are off',
              );
            }

            this.notif = 1;
            this.checkLocation();
          } else if (isEnabled === false && enabled === true) {
            // console.log('BT Mati');
            this.checkLocation();
            // this.notificationManager.cancelAllNotification();
            if (this.notif === 0) {
              this.sendNotification(
                language === 'id' ? 'Bluetooth mati' : 'Bluetooth is off',
              );
            }

            this.notif = 1;
          } else if (isEnabled === true && enabled === false) {
            // console.log('Gps Mati');
            this.checkLocation();
            if (this.notif === 0) {
              this.sendNotification(
                language === 'id'
                  ? 'Gps location anda mati'
                  : 'Gps location is off',
              );
            }
            // this.notificationManager.cancelAllNotification();
            this.notif = 1;
          } else {
            this.localNotify.cancelAllNotification();
            // console.log('BT Gps Nyala');
            this.notif = 0;
          }
        });
      }
    }

    if (prevProps.respTrackingData !== this.props.respTrackingData) {
      if (this.props.respTrackingData.bersinggungan !== null) {
        this.setState({
          dataTrackingData: this.props.respTrackingData.bersinggungan,
        });
        // AsyncStorage.setItem('LastUpdate', CurrentTime);
        BackgroundGeolocation.start();
      }
    }

    if (prevProps.respUpdateApp !== this.props.respUpdateApp) {
      const data = this.props.respUpdateApp.data;
      // console.log('REST respUpdateApp', data);
      if (data != undefined) {
        this.setState({
          LinkIOS: data.link_download_ios,
          LinkAndroid: data.link_download_android,
        });
        this.checkVersion(data.versi_android, data.versi_ios);
      }
    }

    if (prevProps.respRiwayat !== this.props.respRiwayat) {
      // alert(JSON.stringify(this.props.respRiwayat));
      this.setState({urlRiwayat: this.props.respRiwayat.url});
      this.props.navigation.navigate('RiwayatInteraksi', {
        url: this.props.respRiwayat.url,
      });
    }

    if (prevProps.gantiBahasa !== this.props.gantiBahasa) {
      this.setState({language: this.props.gantiBahasa});
    }
  }

  getUrlHistory = async () => {
    await this.props.getRiwayatInterkasi();
  };

  goToHealthDeclaration() {
    this.props.navigation.navigate('HealthDeclaration', {
      url:
        'https://frost.toyota.co.id/jw/web/userview/covid19_response/c19_userview/_/D4F3FF511F874420BEEF0E009947FFD2',
    });
  }

  checkVersion = (android, ios) => {
    const {vAndroid, vIOS, LinkAndroid, LinkIOS} = this.state;
    if (Platform.OS === 'ios') {
      if (vIOS < ios) {
        // alert('update IOS');
        this.setState({
          updateIOS: true,
        });
        // <ModatUpdate/>
      }
    } else {
      if (vAndroid < android) {
        this.setState({
          updateANDROID: true,
        });
        // alert('update ANDROID');
      }
    }
  };

  restTrace = async (lastData, newData, jml) => {
    // console.log('lastData ', lastData, 'newData ', newData, 'jml ', jml);
    if (parseInt(jml) == 0) {
      this.setState({
        TrackingMode: false,
      });
    } else if (parseInt(jml) > 0 && parseInt(jml) < 2) {
      this.setState({
        TrackingMode: true,
        AreaUmum: false,
      });
    } else if (parseInt(jml) > 2) {
      this.setState({
        TrackingMode: true,
        AreaUmum: true,
      });
    }
  };

  checkLocation = async () => {
    const isEnabled = await BluetoothSerial.isEnabled();
    await AsyncStorage.setItem('GpsBt', 'true');
    DeviceInfo.isLocationEnabled().then(enabled => {
      if (isEnabled == false || enabled == false) {
        this.props.navigation.navigate('ModalPermissions', {
          statusBT: isEnabled,
          statusGPS: enabled,
        });
      }
    });
  };

  async componentWillUnmount() {
    console.log('HOME MOUNTED');
    BackgroundGeolocation.removeAllListeners();
    await AsyncStorage.removeItem('AreaUmum');
    await AsyncStorage.removeItem('notify');
    const backgroundService = await AsyncStorage.getItem('BackgroundService');
    // console.log('[WillMount?]', backgroundService)
    if (backgroundService === 'true') {
      // console.log('[run in if]', backgroundService)
      this.backgroundTask();
      this.props.requestTraceFitur(backgroundService);
    } else if (backgroundService === 'false') {
      // console.log('[run in else if]', backgroundService)
      await this.stopTask();
      await BackgroundGeolocation.stop();
      this.props.requestTraceFitur(backgroundService);
    }
  }

  checkBluetooth = async () => {
    const isEnabled = await BluetoothSerial.isEnabled();
    statusBT = await this.setState({statusBT: isEnabled});
  };

  SwitchStatusToogle = val => {
    // console.log('[switch value?]', val)
    this.setState({tracingStatus: val});
    if (val === false) {
      AsyncStorage.setItem('BackgroundService', 'false');
      this.setState({tracingStatus: false});
      this.props.requestTraceFitur(val);
    } else {
      AsyncStorage.setItem('BackgroundService', 'true');
      this.setState({tracingStatus: true});
      this.props.requestTraceFitur(val);
      this.props.setVersion();
    }
  };


  _startScanBle = async () => {
    console.log('[run start ble]');
    SpecialBle.setConfig({
      serviceUUID: await AsyncStorage.getItem('service_id'),
      scanDuration: 5000,
      scanInterval: 2000,
      advertiseInterval: 3000,
      advertiseDuration: 7000,
      advertiseMode: 1,
      token: 'default_token',
      scanMatchMode: 1,
      advertiseTXPowerLevel: 1,
      notificationTitle: 'b',
      notificationContent: 'a'
    });
    SpecialBle.getConfig((config) => {
      console.log('[config ble]', config);
    })
    // SpecialBle.stopBLEService();
    SpecialBle.startBLEService();

    // this._getDevices();
  }

  _getDevices = async () => {
    await SpecialBle.getAllDevices((devices) => {
      // setDevices(devices)
      console.log('[device scan ble]', devices)
    })
  }

  // _key = () => {
  //   let publicKeys = ['12345', '12346', '12347', '12348', '12349']
  //   SpecialBle.setPublicKeys(publicKeys);
  // }

  _getContactsScans = async () => {
    SpecialBle.getScansByKey(pubKey, (scans) => {
        setScans(scans)
    })
  }

  render() {
    const {navigation} = this.props;
    const {
      TrackingMode,
      dataTracking,
      dataTrackingData,
      AreaUmum,
      appOn,
      language,
    } = this.state;
    const {loadingTracing} = this.props;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFF'}}>
        {this.props.network ? (
          Snackbar.dismiss()
        ) : (
          Snackbar.show({
            text: language == 'id' ? 'Tidak Ada Koneksi!' : 'No Internet Connection!',
            duration: Snackbar.LENGTH_INDEFINITE,
          })
        )}
        <StatusCard
          tracingStatus={this.state.tracingStatus}
          SwitchStatus={() =>
            this.SwitchStatusToogle(!this.state.tracingStatus)
          }
          TrackingMode={TrackingMode}
          dataTrackingData={dataTrackingData}
          data={dataTracking}
          AreaUmum={AreaUmum}
          appOn={appOn}
          LastUpdate={
            this.state.lastUpdate ? this.state.lastUpdate : CurrentTime
          }
          language={language}
          loading={loadingTracing}
        />
        <View
          style={{
            height: getHeight(54),
            backgroundColor: '#fff',
            paddingHorizontal: getWidth(8),
            paddingTop: getHeight(3),
          }}>
          <Text
            allowFontScaling={false}
            style={{
              color: '#000000',
              fontWeight: 'bold',
              fontSize: getWidth(4.5),
              textAlign: 'justify',
            }}>
            {language == 'id'
              ? `Untuk memudahkan internal kontrol aktifkan selalu Bluetooth dan GPS`
              : `To facilitate internal control, always activate Bluetooth and GPS`}
          </Text>
          <Text
            allowFontScaling={false}
            style={{
              color: '#393939',
              marginTop: getHeight(2),
              textAlign: 'justify',
            }}>
            {language == 'id'
              ? `Ini upaya kita untuk membantu memudahkan melakukan contact tracing.`
              : `This is our effort to help for contact tracing.`}
          </Text>
          <TouchableOpacity onPress={() => this.onShare()}>
            <Text
              allowFontScaling={false}
              style={{
                color: '#1E378D',
                marginTop: getHeight(2),
                marginBottom: getHeight(2.7),
                fontWeight: 'bold',
              }}>
              {language == 'id' ? `Bagikan aplikasi` : `Share application`}
            </Text>
          </TouchableOpacity>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <CardItem
              icon={HomeImage.Home_BT2}
              title={
                language == 'id'
                  ? 'Riwayat Interaksi'
                  : 'History of Interaction'
              }
              onPress={() => this.getUrlHistory()}
            />
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <CardItem
              icon={HomeImage.Home_List}
              title={
                language == 'id' ? 'Formulir Kesehatan' : 'Health Declaration'
              }
              onPress={() => this.goToHealthDeclaration()}
            />
          </View>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <CardItem
              icon={HomeImage.Home_INF}
              title={language == 'id' ? 'Informasi' : 'Information'}
              onPress={() => navigation.navigate('Informasi')}
            />
          </View>
        </View>

        {this.state.updateANDROID ? (
          <ModatUpdateAndroid
            modalVisible={this.state.updateANDROID}
            url={this.state.LinkAndroid}
          />
        ) : null}
        {this.state.updateIOS ? (
          <ModatUpdateIOS
            modalVisible={this.state.updateIOS}
            url={this.state.LinkIOS}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  respTracking: state.home.resHomeOk,
  respTrackingData: state.trackingData.resTraceOk,
  respSaveCard: state.saveCard.respSaveOk,
  respUpdateApp: state.updateApp.respUpdateOk,
  respRiwayat: state.RiwayatInteraksi.resRiwayatOk,
  gantiBahasa: state.GantiBahasa.gantiBahasaOk,
  respTraceFitur: state.TraceFitur.traceFiturOk,
  updateOk: state.setVersion.updateOk,
  loadingTracing: state.home.loading,
  network: state.network.isConnected
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTracking,
      getPerusahaan,
      getHubungan,
      getTrackingData,
      saveCardAction,
      getUpdateApp,
      getRiwayatInterkasi,
      requestTraceFitur,
      setVersion,
      recordTracing
    },
    dispatch,
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);

const CardItem = ({title, icon, onPress}) => (
  <TouchableOpacity
    style={{
      borderWidth: 1.5,
      borderColor: '#fff',
      backgroundColor: '#FCECEC',
      flexDirection: 'row',
      backgroundColor: '#fff',
      height: getHeight(7.5),
      width: getWidth(85.1),
      borderRadius: getWidth(4),
      marginBottom: getHeight(2.3),
    }}
    onPress={onPress}>
    <View
      style={{
        backgroundColor: '#FCECEC',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(14.5),
        borderTopLeftRadius: getWidth(3.8),
        borderBottomLeftRadius: getWidth(3.8),
      }}>
      <Image
        style={{
          height:
            icon == require('../../assets/image/iconList.png')
              ? getHeight(5.3)
              : getHeight(3.3),
          width:
            icon == require('../../assets/image/iconList.png')
              ? getHeight(6.3)
              : getHeight(3.3),
          bottom: icon == require('../../assets/image/HOME_INF.png') ? 2 : 0,
          left: 5,
        }}
        source={icon}
        resizeMode="contain"
      />
    </View>
    <View
      style={{
        width: getWidth(45),
        justifyContent: 'center',
        backgroundColor: '#FCECEC',
      }}>
      <Text
        allowFontScaling={false}
        style={{
          color: '#000',
          fontSize: getWidth(4.3),
        }}>
        {title}
      </Text>
    </View>
    <View
      style={{
        backgroundColor: '#FCECEC',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(25),
        borderTopRightRadius: getWidth(4),
        borderBottomRightRadius: getWidth(4),
      }}>
      <View style={{justifyContent: 'center', alignItems: 'center'}} />
    </View>
  </TouchableOpacity>
);

const ModatUpdateAndroid = ({modalVisible, url}) => (
  <Modal
    animationType="slide"
    transparent={true}
    visible={modalVisible}
    onRequestClose={() => {
      // Alert.alert('Modal has been closed.');
    }}>
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <Text allowFontScaling={false} style={styles.modalText}>
          {`Update Aplikasi Around Us Anda Sekarang!`}
        </Text>

        <TouchableHighlight
          style={{...styles.openButton, backgroundColor: '#2196F3'}}
          onPress={() => {
            Linking.openURL(url);
          }}>
          <Text allowFontScaling={false} style={styles.textStyle}>
            UPDATE{' '}
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  </Modal>
);

const ModatUpdateIOS = ({modalVisible, url}) => (
  <Modal
    animationType="slide"
    transparent={true}
    visible={modalVisible}
    onRequestClose={() => {
      // Alert.alert('Modal has been closed.');
    }}>
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <Text allowFontScaling={false} style={styles.modalText}>
          {`Update Aplikasi Around Us Anda Sekarang!`}
        </Text>

        <TouchableHighlight
          style={{...styles.openButton, backgroundColor: '#2196F3'}}
          onPress={() => {
            Linking.openURL(url);
          }}>
          <Text allowFontScaling={false} style={styles.textStyle}>
            UPDATE{' '}
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  </Modal>
);

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
