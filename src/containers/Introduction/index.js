import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {getHeight, getWidth} from '../../helper/init';
import {IntroImage, Flag} from '../../helper/image';
import {colors} from '../../helper/color';

import RoundedButton from '../../components/RoundedButton';
import RoleModal from '../../components/RoleModal';
import ModalLanguage from '../../components/ModalLanguage';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';

class Introduction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleRole: false,
      visibleLanguage: false,
      language: '',
    };
  }

  async componentDidMount() {
    let totalLanguage = RNLocalize.getLocales().length;
    var language = 'id'
    // if (totalLanguage == 1) {
    //   var language = RNLocalize.getLocales()[0].languageCode;
    // } else {
    //   var language = RNLocalize.getLocales()[1].languageCode;
    // }

    SplashScreen.hide();
    let languageNow = await AsyncStorage.getItem('language');
    if (!languageNow) {
      await AsyncStorage.setItem('language', language);
    }
    this.setState({language});
    // console.log('[DidMount][bahasa]', languageNow)
  }
  showRole = () => {
    const {visibleRole} = this.state;
    this.setState({
      visibleRole: !visibleRole,
    });
  };
  showLanguage = text => {
    const {visibleLanguage} = this.state;
    if (!text) {
      this.setState({
        visibleLanguage: !visibleLanguage,
      });
    } else {
      this.setState({
        visibleLanguage: text,
      });
    }
  };

  changeLanguage = async language => {
    await AsyncStorage.setItem('language', language);
    this.setState({language});
  };
  visitorPress = () => {
    const {navigation} = this.props;
    const {visibleRole} = this.state;
    this.setState({visibleRole: !visibleRole});
    navigation.push('RegisterVisitor');
    AsyncStorage.setItem('role', 'visitor');
  };
  employeePress = () => {
    const {navigation} = this.props;
    const {visibleRole} = this.state;
    this.setState({visibleRole: !visibleRole});
    navigation.push('RegisterKaryawan', {whoThis: 'employee'});
    AsyncStorage.setItem('role', 'employee');
  };
  render() {
    const {navigation} = this.props;
    const {visibleRole, language, visibleLanguage} = this.state;
    return (
      <View style={styles.container}>
        <Image style={styles.introIlustrasi} source={IntroImage.IntroIlust} />
        <View style={styles.containerTitle}>
          <Text style={styles.introTitle}>
            {language == 'id'
              ? `Penjagaan jarak untuk physical distancing`
              : `Keep physical distance distancing`}
          </Text>
        </View>

        <View style={styles.containerContent}>
          <Text style={styles.introContent}>
            {language == 'id'
              ? `Aplikasi hanya menggunakan sinyal bluetooth untuk mengetahui jarak antar pengguna Aplikasi Around Us.`
              : `The application only uses Bluetooth signals to find out the distance between users of Around Us Applications.`}
          </Text>

          <Text style={[styles.introContent, {marginTop: 20}]}>
            {language == 'id'
              ? `Aplikasi ini tidak mengambil data personal yang lain (sepertihalnya daftar kontak dan alamat) atau data lokasi. Data yang lebih dari 21 hari akan otomatis dihapus.`
              : `This application does not retrieve other personal data (such as contact lists and addresses) or location data. Data that is more than 21 days will be automatically deleted.`}
          </Text>
        </View>
        <RoundedButton
          onPress={() => this.showRole()}
          // onPress={() => navigation.push('Test')}
          title={language == 'id' ? 'Bergabung' : 'Join Us'}
          bottom={'37.5%'}
        />
        <RoleModal
          language={language}
          visible={visibleRole}
          onClose={() => this.showRole()}
          visitorPress={() => this.visitorPress()}
          employeePress={() => this.employeePress()}
        />
        <ModalLanguage
          language={language}
          visible={visibleLanguage}
          onClose={text => this.showLanguage(text)}
          onPress={text => this.changeLanguage(text)}
        />
        <View
          style={{position: 'absolute', right: getWidth(6), top: getHeight(3)}}>
          <TouchableOpacity
            // style={{alignSelf: 'center'}}
            onPress={() => this.showLanguage()}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 0.1,
                bordercolor: '#707070',
                paddingVertical: 4,
                paddingHorizontal: 8,
                borderRadius: 8,
                marginBottom: 2,
              }}>
              <Text>{language == 'id' ? `Bahasa` : `Language`}</Text>
              <Image
                source={language == 'id' ? Flag.IND : Flag.ENG}
                style={{
                  marginLeft: 10,
                  width: getWidth(5),
                  height: getWidth(5),
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default Introduction;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  introIlustrasi: {
    width: '70%',
    height: '70%',
    resizeMode: 'contain',
    top: -90,
  },
  containerTitle: {
    top: '-25%',
    marginHorizontal: '8%',
  },
  introTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    // textAlign: 'justify'
    // fontFamily: 'Cairo-Bold'
  },
  containerContent: {
    top: '-11%',
    marginHorizontal: '8%',
  },
  introContent: {
    color: colors.charcoal,
    fontSize: 14,
    textAlign: 'justify',
    // fontFamily: 'Cairo-Regular'
  },
});
