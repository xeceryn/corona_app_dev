import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { SafeAreaView, } from 'react-native-safe-area-context'
import { getHeight, getWidth } from '../../helper/init'
import { HomeImage, EmergencyImage } from '../../helper/image'
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modal';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

export default class Informasi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            radio_value :[
                { label: 'SEHAT', value: 0 },
                { label: 'ODP', value: 1 },
                { label: 'PDP', value: 2 },
                { label: 'BERESIKO', value: 3 },
                { label: 'TERJANGKIT', value: 4 }
            ],
            value: 0,
        };
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', height: getHeight(45) }}>
                    <Round />
                    <Image style={{ height: getHeight(35), width: getHeight(35) }} source={EmergencyImage.EM_IC} resizeMode='contain' />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', height: getHeight(55) }}>
                    <ScrollView>
                        <CardItem
                            title='Layanan Darurat'
                            subtitle='Hotline 199 ext 9'
                            icon={EmergencyImage.Umum}
                        />
                        <CardItem
                            title='Laporkan Status'
                            subtitle='Sehat, ODP, PDP, Beresiko'
                            onPress={() => this.setState({ isVisible: !this.state.isVisible })}
                            icon={EmergencyImage.Status}
                        />
                        <CardItem
                            title='Whatsapp '
                            subtitle='08123172'
                            icon={EmergencyImage.Call}
                        />
                    </ScrollView>
                </View>

                <Modal
                    isVisible={this.state.isVisible}
                    customBackdrop={<View style={{ flex: 1, backgroundColor: '#484848' }} />}>
                    <View style={{ height: getHeight(45), width: getWidth(90), backgroundColor: '#FFFFFF', borderRadius: getWidth(5), padding: getWidth(4), }}>
                        <View>
                            <Text style={{ fontWeight: 'bold', fontSize: getWidth(6), marginBottom: getHeight(6) }}>{`Laporkan`}</Text>
                            <RadioForm
                                radio_props={this.state.radio_value}
                                initial={this.state.value}
                                // labelColor={'#D03F3F'}
                                buttonColor={'#D03F3F'}
                                selectedButtonColor={'#D03F3F'}
                                onPress={(value) => { this.setState({ value: value }) }}
                            />
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0, backgroundColor: '#D03F3F', width: getWidth(90), borderBottomLeftRadius: getWidth(5), borderBottomRightRadius: getWidth(5) }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ isVisible: !this.state.isVisible })}
                                style={{ width: getWidth(90), height: getHeight(6), justifyContent: 'center', alignItems: 'center', borderBottomLeftRadius: getWidth(5), borderBottomRightRadius: getWidth(5) }}
                            >
                                <Text style={{ color: '#FFF' }}>{`Laporkan`}</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </Modal>
            </SafeAreaView >
        )
    }
}


const CardItem = ({
    params, title, subtitle, icon, onPress
}) => (
        <View style={{ flexDirection: 'row', backgroundColor: '#fff', height: getHeight(10), width: getWidth(80), borderRadius: getWidth(2), elevation: 1, marginBottom: getHeight(5) }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', width: getWidth(20) }}>
                <Image style={{ height: getHeight(6), width: getHeight(6) }} source={icon} resizeMode='contain' />
            </View>
            <View style={{ width: getWidth(40), justifyContent: 'center' }}>
                <Text>{title}</Text>
                <Text>{subtitle}</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', width: getWidth(20) }}>
                <TouchableOpacity onPress={onPress} style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name="ios-arrow-forward" size={getWidth(5)} />
                </TouchableOpacity>
            </View>

        </View>
    );

export const Round = ({
    params,
}) => (
        <View style={{
            height: getWidth(75),
            width: getWidth(75),
            borderRadius: getWidth(75),
            backgroundColor: '#F9F2F2',
            position: 'absolute',
            left: getWidth(-24),
            top: getWidth(-26)
        }} />
    );


