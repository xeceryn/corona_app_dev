import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Platform,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getHeight, getWidth} from '../../helper/init';
import {PermissionImage} from '../../helper/image';
import {ScrollView} from 'react-native-gesture-handler';
import BleManager from 'react-native-ble-manager';
import Geolocation from '@react-native-community/geolocation';
import ReactNativeLocationServicesSettings from 'react-native-location-services-settings';
import DeviceInfo from 'react-native-device-info';
import { check, PERMISSIONS, request, RESULTS, openSettings } from 'react-native-permissions'

class ModalPermissions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Bluetooth: this.props.route.params.statusBT,
      Gps: true,
      Battery: false,
      PushNotif: false,
      currentLongitude: null,
      currentLatitude: null,
      language: '',
    };
  }

  async componentDidMount() {
    let language = await AsyncStorage.getItem('language');
    this.setState({language});
    this.checkLocation();
    var pushNotif = '';
    var battery = '';
    try {
      pushNotif = await AsyncStorage.getItem('pushNotif');
      battery = await AsyncStorage.getItem('battery');
    } catch (error) {
      console.log('ERROR modal permission: ', error);
    }

    if (pushNotif == 'true') {
      this.setState({PushNotif: true});
    } else {
      this.setState({PushNotif: false});
    }
    if (battery == 'true') {
      this.setState({Battery: true});
    } else {
      this.setState({Battery: false});
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState != this.state) {
      if (
        this.state.Bluetooth &&
        this.state.Gps &&
        this.state.Battery &&
        this.state.PushNotif
      ) {
        this.props.navigation.goBack();
      }
    }
  }

  batteryPermission = () => {
    const {language} = this.state;
    Alert.alert(
      language == 'id'
        ? 'Around Us ingin mengaktifkan Battery Optimization'
        : 'Around Us wants to activate Battery Optimization',
      '',
      [
        {
          text: 'TOLAK',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'IZINKAN',
          onPress: () => this.batteryHandler(),
        },
      ],
      {cancelable: false},
    );
  };

  PushPermission = () => {
    const {language} = this.state;
    Alert.alert(
      language == 'id'
        ? 'Around Us ingin mengaktifkan Push Notification'
        : 'Around Us wants to activate Push Notification',
      '',
      [
        {
          text: 'TOLAK',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'IZINKAN',
          onPress: () => this.pushNotificationHandler(),
        },
      ],
      {cancelable: false},
    );
  };

  pushNotificationHandler = async () => {
    await AsyncStorage.setItem('pushNotif', 'true');
    this.setState({PushNotif: !this.state.PushNotif});
  };

  batteryHandler = async () => {
    await AsyncStorage.setItem('battery', 'true');
    this.setState({Battery: !this.state.Battery});
  };

  bluetoothHandler = () => {
    var that = this;
    if(Platform.OS === 'ios') {
      async function requestBluetooth() {
        try {
          const res = await check(PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL)
          if (res === RESULTS.DENIED) {
            const res2 = await request(PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL)
            if(res2 === RESULTS.GRANTED) {
              that.setState({ Bluetooth: true })
            } else if(res2 === RESULTS.UNAVAILABLE) {
              console.log('[res]', RESULTS)
              openSettings()
              that.setState({ Bluetooth: true })
            }
          }
        } catch (err) {
          that.setState({ Bluetooth: false })
          console.log('[err BT]', err)
        }
      }
      requestBluetooth()
    } else {
      BleManager.start({showAlert: false}).then(() => {
        BleManager.enableBluetooth()
          .then(() => {
            that.setState({Bluetooth: true});
          })
          .catch(e => {});
      });
    }
  };

  checkLocation = () => {
    DeviceInfo.isLocationEnabled().then(enabled => {
      // console.log('checkLocation ', enabled)
      this.setState({Gps: enabled});
    });
  };

  locationHandler = () => {
    ReactNativeLocationServicesSettings.checkStatus('high_accuracy').then(
      res => {
        if (!res.enabled) {
          ReactNativeLocationServicesSettings.askForEnabling(res => {
            if (res) {
              this.setState({Gps: true});
              console.log('location services were allowed by the user');
            } else {
              console.log('location services were denied by the user');
            }
          });
        }
      },
    );
  };

  callLocation(that) {
    //alert("callLocation Called");
    Geolocation.getCurrentPosition(
      //Will give you the current location
      position => {
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json
        that.setState({currentLongitude: currentLongitude});
        //Setting state Longitude to re re-render the Longitude Text
        that.setState({currentLatitude: currentLatitude});
        //Setting state Latitude to re re-render the Longitude Text
      },
      error => console.log(error.message),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
    that.watchID = Geolocation.watchPosition(position => {
      //Will give you the location on location change
      // console.log(position);
      const currentLongitude = JSON.stringify(position.coords.longitude);
      //getting the Longitude from the location json
      const currentLatitude = JSON.stringify(position.coords.latitude);
      //getting the Latitude from the location json
      that.setState({currentLongitude: currentLongitude});
      //Setting state Longitude to re re-render the Longitude Text
      that.setState({currentLatitude: currentLatitude});
      //Setting state Latitude to re re-render the Longitude Text
    });
  }

  render() {
    const {Bluetooth, Gps, Battery, PushNotif, language} = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFF'}}>
        <View
          style={{
            position: 'absolute',
            top: getWidth(-25),
            left: getWidth(-32),
            height: getWidth(80),
            width: getWidth(80),
            borderRadius: getWidth(80),
            backgroundColor: '#FAECEC',
          }}
        />
        <View
          style={{
            position: 'absolute',
            top: getWidth(28),
            right: getWidth(-25),
            height: getWidth(50),
            width: getWidth(50),
            borderRadius: getWidth(50),
            backgroundColor: '#FAECEC',
          }}
        />
        <View
          style={{
            position: 'absolute',
            bottom: getWidth(-18),
            left: getWidth(-32),
            height: getWidth(80),
            width: getWidth(80),
            borderRadius: getWidth(80),
            backgroundColor: '#FAECEC',
          }}
        />
        <View
          style={{
            height: getHeight(40),
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: getHeight(27), width: getHeight(27)}}
            source={PermissionImage.Modal_Perm}
            resizeMode="contain"
          />
        </View>
        <View style={{height: getHeight(13), paddingHorizontal: getWidth(6.5)}}>
          <Text
            style={{
              // fontFamily: 'Cairo-Bold',
              fontWeight: 'bold',
              fontSize: getWidth(5),
            }}>
            {language == 'id'
              ? `Oops salah satu pengaturan mati`
              : `Oops, one of the settings is off`}
          </Text>
          <Text
            style={{
              // fontFamily: 'Cairo-Regular',
              color: '#97A7B4',
            }}>
            {language == 'id'
              ? `Segera aktifkan pengaturan dibawah ini untuk menggunakan fitur secara penuh`
              : `Immediately activate the settings below to use the full feature`}
          </Text>
        </View>
        <View style={{height: getHeight(40), paddingHorizontal: getWidth(6.5)}}>
          <ScrollView>
            <CardItem
              title={`Bluetooth:${Bluetooth ? 'On' : 'OFF'}`}
              status={Bluetooth}
              onPress={!Bluetooth ? () => this.bluetoothHandler() : null}
            />
            <CardItem
              title={`Location Permission:${Gps ? 'On' : 'OFF'}`}
              status={Gps}
              onPress={!Gps ? () => this.locationHandler() : null}
            />
            <CardItem
              title={`Battery Optimisation:${Battery ? 'On' : 'OFF'}`}
              onPress={() => this.batteryPermission()}
              status={Battery}
            />
            <CardItem
              title={`Push Notification:${PushNotif ? 'On' : 'OFF'}`}
              onPress={() => this.PushPermission()}
              status={PushNotif}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default ModalPermissions;

const CardItem = ({title, onPress, status}) => (
  <TouchableOpacity
    style={{
      borderWidth: 0.1,
      borderColor: '#707070',
      flexDirection: 'row',
      backgroundColor: '#fff',
      height: getHeight(7),
      width: getWidth(86.8),
      borderRadius: getWidth(2),
      elevation: 1,
      marginBottom: getHeight(2),
    }}
    onPress={onPress}>
    <View
      style={{
        width: getWidth(65),
        justifyContent: 'center',
        padding: getWidth(4.5),
      }}>
      <Text
        style={{
          color: '#000',
          fontSize: getWidth(4),
          // fontFamily: 'Cairo-SemiBold',
          fontWeight: 'bold',
        }}>
        {title}
      </Text>
    </View>
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(20),
      }}>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: status ? '#3FD081' : '#D03F3F',
            width: getWidth(6.5),
            height: getWidth(6.5),
            borderRadius: getWidth(6.5),
          }}>
          <Text
            style={{
              color: '#FFF',
              fontSize: getWidth(2.5),
              // fontFamily: 'Cairo-Regular',
            }}>
            {status ? `OK` : `OFF`}
          </Text>
        </View>
      </View>
    </View>
  </TouchableOpacity>
);
