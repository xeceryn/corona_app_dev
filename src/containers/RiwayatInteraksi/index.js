import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Platform} from 'react-native';
import {WebView} from 'react-native-webview';
import {getHeight, getWidth} from '../../helper/init';
uuidv4();

class RiwayatInteraksi extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}} pointEvents="none">
        {Platform.OS == 'ios' ? (
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Text>BackButton</Text>
          </TouchableOpacity>
        ) : null}
        <WebView
          source={{
            uri: this.props.route.params.url,
          }}
          style={{
            height: getHeight(100),
            width: getWidth(100),
            backgroundColor: '#fff',
          }}
        />
      </View>
    );
  }
}

export default RiwayatInteraksi;
