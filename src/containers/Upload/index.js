import React from 'react'
import { Text, View, Switch } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { getHeight, getWidth } from '../../helper/init'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'

export default function Upload() {
    return (
        <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ padding: getWidth(5), height: getHeight(10) }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: getHeight(3) }}>{`Upload Data Online`}</Text>
                    <Switch />
                </View>
                <View>
                    <Text>{`Upload Data Realtime Secara Online`}</Text>
                </View>
            </View>
            <View style={{ padding: getWidth(5), height: getHeight(62) }}>
                <View style={{ marginBottom: getHeight(3) }}>
                    <Text style={{ fontWeight: 'bold', fontSize: getHeight(4) }}>{`Upload Data Anda`}</Text>
                </View>
                <View>
                    <ScrollView>
                        <Text style={{ textAlign: 'justify' }}>{`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in quam nisl. Proin consequat augue augue, in consectetur nulla consequat nec. Fusce at ex nec leo condimentum maximus. Suspendisse venenatis metus vulputate velit iaculis varius. Suspendisse consequat ac elit sit amet fringilla. Donec a erat a sem dictum vehicula ac ut odio. Etiam et nunc et est posuere malesuada. Vestibulum vitae luctus arcu. Morbi lobortis dolor nulla, quis posuere sem pharetra nec. In condimentum mauris massa, id malesuada nibh commodo ut. Morbi ornare est id gravida interdum. Maecenas non elit sed nulla varius mattis nec eu massa. Suspendisse vitae lectus semper, maximus mauris a, vestibulum lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in quam nisl. Proin consequat augue augue, in consectetur nulla consequat nec. Fusce at ex nec leo condimentum maximus. Suspendisse venenatis metus vulputate velit iaculis varius. Suspendisse consequat ac elit sit amet fringilla. Donec a erat a sem dictum vehicula ac ut odio. Etiam et nunc et est posuere malesuada. Vestibulum vitae luctus arcu. Morbi lobortis dolor nulla, quis posuere sem pharetra nec. In condimentum mauris massa, id malesuada nibh commodo ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in quam nisl. Proin consequat augue augue, in consectetur nulla consequat nec. Fusce at ex nec leo condimentum maximus. Suspendisse venenatis metus vulputate velit iaculis varius. Suspendisse consequat ac elit sit amet fringilla. Donec a erat a sem dictum vehicula ac ut odio. Etiam et nunc et est posuere malesuada. Vestibulum vitae luctus arcu. Morbi lobortis dolor nulla, quis posuere sem pharetra nec. In condimentum mauris massa, id malesuada nibh commodo ut. Morbi ornare est id gravida interdum. Maecenas non elit sed nulla varius mattis nec eu massa. Suspendisse vitae lectus semper, maximus mauris a, vestibulum lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in quam nisl. Proin consequat augue augue, in consectetur nulla consequat nec. Fusce at ex nec leo condimentum maximus. Suspendisse venenatis metus vulputate velit iaculis varius. Suspendisse consequat ac elit sit amet fringilla. Donec a erat a sem dictum vehicula ac ut odio. Etiam et nunc et est posuere malesuada. Vestibulum vitae luctus arcu. Morbi lobortis dolor nulla, quis posuere sem pharetra nec. In condimentum mauris massa, id malesuada nibh commodo ut.`}</Text>
                    </ScrollView>
                </View>

            </View>
            <View style={{ height: getHeight(10), position: 'absolute', bottom: 0, paddingHorizontal: getWidth(10) }}>
                <TouchableOpacity
                    onPress={() => alert('coming soon !')}
                    style={{ backgroundColor: '#E2374F', width: getWidth(80), height: getHeight(5), justifyContent: 'center', alignItems: 'center', borderRadius: getWidth(3) }}>
                    <Text style={{ color: '#fff' }}>{`Setuju & Upload`}</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}