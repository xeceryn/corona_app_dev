import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Text,
  View,
  Image,
  Animated,
  Alert,
  Switch,
  BackHandler,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getHeight, getWidth, formatPhoneNumber} from '../../helper/init';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {getInformasi} from '../../store/action/informasi';
import {requestGantiBahasa} from '../../store/action/GantiBahasa';
import {requestTraceFitur} from '../../store/action/traceFitur';
import AsyncStorage from '@react-native-community/async-storage';
import {Flag, ArrowIcon, Settings} from '../../helper/image';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import BackgroundJob from 'react-native-background-actions';
import Snackbar from 'react-native-snackbar'

class Informasi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      role: null,
      menu: '',
      perusahaanList: null,
      hubunganList: null,
      language: '',
      statusTracing: null,
      lastLogin: ''
    };
  }

  async componentDidMount() {
    let language = await AsyncStorage.getItem('language');
    let lastLogin = await AsyncStorage.getItem('lastLogin')
    this.setState({language, lastLogin});
    this.getRole();
    await this.props.getInformasi();
    this.setState({perusahaanList: this.props.resPerusahaanOk.data});
    this.setState({hubunganList: this.props.resHubunganOk.data});
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.resInformasiOk != this.props.resInformasiOk) {
      this.setState({
        profile: this.props.resInformasiOk.profile,
        menu: this.props.resInformasiOk.menu,
        statusTracing: this.props.resInformasiOk.profile.status_tracing,
      });
    }

    if (prevProps.gantiBahasa != this.props.gantiBahasa) {
      this.setState({language: this.props.gantiBahasa});
      this.props.getInformasi();
    }

    if (prevProps.respTraceFitur != this.props.respTraceFitur) {
      this.setState({
        statusTracing: this.state.statusTracing == 1 ? 0 : 1,
      });

      AsyncStorage.setItem("BackgroundService", this.props.respTraceFitur.status_tracing.toString() )
    }
  }

  getRole = async () => {
    await AsyncStorage.getItem('role', (error, result) => {
      if (result) {
        // alert(result);
        this.setState({
          role: result,
        });
      }
    });
  };

  filterHubKeluarga = id => {
    const HubKel = this.state.hubunganList.filter(item => item.id === id);
    if (HubKel[0]) {
      return HubKel[0].hubungan;
    } else {
      return 'Kerabat / Orang Lain';
    }
  };

  filterPerusahaan = id => {
    const company = this.state.perusahaanList.filter(item => item.id === id);
    return company[0].company_name;
  };

  setStatusTrace = val => {
    this.props.requestTraceFitur(val);
  };

  alertLogout = () => {
    const { language } = this.state
    Alert.alert(
      'Around Us',
      language === 'id' ? 
      'Apa Anda yakin Keluar aplikasi?'
      : 'Are you sure Logout Aplication?',
      [
        { text: language === 'id' ? 'Batal' : 'Cancel', onPress: () => console.log('Cancel Pressed') },
        { text: 'Oke', onPress: () => this.clearSession() }
      ],
    );
  }
  clearSession = async () => {
    await AsyncStorage.removeItem('token')
    await AsyncStorage.removeItem('BackgroundService')
    await BackgroundGeolocation.stop()
    await BackgroundJob.stop()
    // this.props.navigation.navigate('Introduction', {back: true})
    BackHandler.exitApp()
  }
  render() {
    const {language, menu} = this.state;
    return (
      <SafeAreaView
        style={{flex: 1, alignItems: 'center', backgroundColor: '#FFF'}}>
        {this.props.network ? (
          Snackbar.dismiss()
          ) : (
          Snackbar.show({
            text: language == 'id' ? 'Tidak Ada Koneksi!' : 'No Internet Connection!',
            duration: Snackbar.LENGTH_INDEFINITE,
          })
        )}
        <View
          style={{
            backgroundColor: '#F9F2F2',
            width: getWidth(60),
            height: getWidth(60),
            borderRadius: getWidth(60),
            position: 'absolute',
            left: getWidth(-20),
            top: getWidth(-20),
          }}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <ProfileCard
            language={language}
            role={this.state.role}
            params={this.state.profile}
            lastLogin={this.state.lastLogin}
            hubungan={
              this.state.hubunganList
                ? this.filterHubKeluarga(this.state.profile.anggota_keluarga_id)
                : 'undefined'
            }
            onAction={val => this.setStatusTrace(val)}
            statusTracing={this.state.statusTracing}
            onPress={() => this.alertLogout()}
          />
          <BahasaCard
            language={language}
            action={language => {
              this.props.requestGantiBahasa(language);
            }}
          />
          {menu !== '' && menu.map((item, index) => (
            <AccordianCard key={index} params={item} />
          ))}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  resInformasiOk: state.informasi.resInformasiOk,
  resPerusahaanOk: state.perusahaan.resPerusahaanOk,
  resHubunganOk: state.hubKeluarga.reshubKeluargaOk,
  gantiBahasa: state.GantiBahasa.gantiBahasaOk,
  respTraceFitur: state.TraceFitur.traceFiturOk,
  network: state.network.isConnected,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getInformasi,
      requestGantiBahasa,
      requestTraceFitur,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Informasi);

const ProfileCard = ({
  params,
  hubungan,
  role,
  language,
  onAction,
  statusTracing,
  onPress,
  lastLogin
}) => (
  <View
    style={{
      borderColor: '#FCECEC',
      backgroundColor: '#FCECEC',
      borderWidth: 0.1,
      width: getWidth(90),
      marginVertical: getHeight(4),
      padding: getWidth(5),
      borderRadius: getWidth(3.5),
      elevation: 1,
    }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text
        style={{
          fontSize: getWidth(4.5),
          fontWeight: 'bold',
        }}>{`Profile`}</Text>
       <TouchableOpacity style={{ alignItems: 'center' }} onPress={onPress}>
         <Image source={Settings.logout} style={{ resizeMode: 'contain', width: 25, height: 25 }} />
          <Text>{language === 'id' ? 'Keluar' : 'Logout'}</Text>
       </TouchableOpacity>
      </View>
    <Text>{`${params.nama}`}</Text>
    <Text
      style={{
        marginBottom: getHeight(1),
      }}>{`${formatPhoneNumber(params.no_hp)}`}</Text>
    {role != 'visitor' ? (
      <View style={{backgroundColor: '#FCECEC'}}>
        <Text>
          {language == 'id' ? `Karyawan` : `The employee`}
        </Text>
        <Text>
          {language == 'id'
            ? `Noreg: ${params.employee_code}`
            : `ID CARD: ${params.employee_code}`}
        </Text>
        <Text>{`Company ID: ${params.perusahaan}`}</Text>
        <Text>{`Login: ${lastLogin}`}</Text>
      </View>
    ) : (
      <View style={{backgroundColor: '#FCECEC'}}>
        <Text>
          {language == 'id' ? `Non Karyawan` : `Visitor`}
        </Text>
        <Text>
          {language == 'id' ? `NIK: ${params.nik}` : `ID CARD: ${params.nik}`}
        </Text>
        <Text>{`Login: ${lastLogin}`}</Text>
      </View>
    )}
  </View>
);

export class AccordianCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      heightCard: new Animated.Value(getHeight()),
    };
  }

  openHadler() {
    this.setState({open: !this.state.open});
  }
  render() {
    return (
      <View
        style={{
          borderColor: '#707070',
          borderWidth: 0.1,
          justifyContent: 'center',
          width: getWidth(90),
          backgroundColor: '#fff',
          marginVertical: getHeight(1.7),
          padding: getWidth(3.5),
          borderRadius: getWidth(3.5),
          elevation: 1,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: getWidth(4.5),
              marginBottom: getWidth(2),
              color: '#2D2D2D',
            }}>{`${this.props.params.title}`}</Text>
          <TouchableOpacity onPress={() => this.openHadler()}>
            {this.state.open ? (
              <Image source={ArrowIcon.UP} />
            ) : (
              <Image source={ArrowIcon.DOWN} />
            )}
          </TouchableOpacity>
        </View>
        {this.state.open ? (
          <Text
            style={{
              textAlign: 'justify'
            }}>{`${this.props.params.desc}`}</Text>
        ) : null}
      </View>
    );
  }
}

const DATA = [
  {
    id: '1',
    title: 'Indonesia',
    flag: Flag.IND,
    language: 'id',
  },
  {
    id: '2',
    title: 'English',
    flag: Flag.ENG,
    language: 'en',
  },
];

export class BahasaCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      heightCard: new Animated.Value(getHeight()),
    };
  }

  openHadler() {
    this.setState({open: !this.state.open});
  }

  changeBahasa = async language => {
    // alert(language);
    let bahasa = language == 'id' ? 'Indonesian' : 'English';

    if (this.props.language == language) {
      Alert.alert(
        'Around Us',
        this.props.language == 'id'
          ? `anda sudah menggunakan bahasa ${bahasa}`
          : `You already use ${bahasa}`,
        [
          {
            text: 'OK',
            onPress: () => {},
          },
        ],
        {cancelable: false},
      );
    } else {
      Alert.alert(
        'Around Us',
        this.props.language == 'id'
          ? `Apakah anda ingin merubah bahasa menjadi ${bahasa}`
          : `Do you want to change the language to ${bahasa}`,
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: async () => {
              this.props.action(language);
            },
          },
        ],
        {cancelable: false},
      );
      await AsyncStorage.setItem('language', language);
    }
  };
  render() {
    return (
      <View
        style={{
          borderColor: '#707070',
          borderWidth: 0.1,
          justifyContent: 'center',
          width: getWidth(90),
          backgroundColor: '#fff',
          marginVertical: getHeight(1.7),
          padding: getWidth(3.5),
          borderRadius: getWidth(3.5),
          elevation: 1,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: getWidth(4.5),
              marginBottom: getWidth(2),
              color: '#2D2D2D',
            }}>
            {this.props.language == 'id' ? `Pilih Bahasa` : `Select Language`}
          </Text>
          <TouchableOpacity onPress={() => this.openHadler()}>
            {this.state.open ? (
              <Image source={ArrowIcon.UP} />
            ) : (
              <Image source={ArrowIcon.DOWN} />
            )}
          </TouchableOpacity>
        </View>
        {this.state.open ? (
          <View>
            {DATA.map((item, index) => (
              <TouchableOpacity
                  key={index}
                  style={{flexDirection: 'row'}}
                  onPress={() => this.changeBahasa(item.language)}>
                  <View
                    style={{
                      width: getWidth(70),
                      padding: 5,
                      borderBottomLeftRadius: 8,
                      borderTopLeftRadius: 8,
                      marginBottom: 5,
                      backgroundColor:
                        item.language == this.props.language
                          ? '#FCECEC'
                          : '#FFF',
                    }}>
                    <Text
                      style={{
                        fontWeight:
                          item.language == this.props.language
                            ? 'bold'
                            : 'normal',
                      }}>
                      {item.title}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: getWidth(10),
                      marginBottom: 5,
                      padding: 5,
                      borderBottomRightRadius: 8,
                      borderTopRightRadius: 8,
                      backgroundColor:
                        item.language == this.props.language
                          ? '#FCECEC'
                          : '#FFF',
                    }}>
                    <Image
                      source={item.flag}
                      style={{height: getWidth(5), width: getWidth(5)}}
                    />
                  </View>
                </TouchableOpacity>
            ))}
          </View>
        ) : null}
      </View>
    );
  }
}

export class SwipeButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSwitch: 1,
    };
  }

  changeValue = val => {
    this.props.onAction(val ? 1 : 0);
    this.setState({activeSwitch: val});
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.statusTracing != prevProps.statusTracing) {
      this.setState({
        activeSwitch: parseInt(this.props.statusTracing) == 0 ? false : true,
      });
    }
    // alert(parseInt(this.props.statusTracing));
  }

  render() {
    const {activeSwitch} = this.state;
    // alert(activeSwitch);
    return (
      <View style={{marginTop: getHeight(2)}}>
        <Switch
          trackColor={{false: '#fff', true: '#81b0ff'}}
          thumbColor={
            activeSwitch ? 'rgba(3, 252, 23, 0.8)' : 'rgba(252, 3, 3, 0.8)'
          }
          ios_backgroundColor="#3e3e3e"
          onValueChange={val => this.changeValue(val)}
          value={activeSwitch}
        />
      </View>
    );
  }
}
