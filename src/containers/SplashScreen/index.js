import React, { Component } from 'react'
import { StatusBar, View, Text, Image, StyleSheet } from 'react-native'

import { SplashImage } from '../../helper/image'
import { colors } from '../../helper/color'

class SplashScreen extends Component {
    componentDidMount() {
        const { navigation } = this.props
        setTimeout(function(){
            navigation.navigate('Introduction')
        }, 1000)
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.red} />
                <Image style={styles.logoApp} source={SplashImage.LogoApp} />
                <Text style={styles.titleSplash}>Around Us</Text>
                <Image style={styles.logoPerusahaan} source={SplashImage.LogoPerusahaan} />
            </View>
        )
    }
}
export default SplashScreen
const styles = StyleSheet.create({
    container: {
        flex:1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: 'white'
    },
    logoApp: {
        width: 200,
        resizeMode: 'contain'
    },
    titleSplash: {
        fontWeight: 'bold',
        fontSize: 40,
        color: colors.red,
        top: -20
    },
    logoPerusahaan: {
        width: 150,
        resizeMode: 'contain',
        bottom: -120
    }
})
