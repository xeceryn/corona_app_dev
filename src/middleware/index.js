import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export default async () => {
  await axios.interceptors.request.use(
    async (config) => {
      const token = await AsyncStorage.getItem('token')
      
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      // console.log('INTERCEPTOR', token)
      return config;
    },
    (error) => Promise.reject(error),
  );
};
